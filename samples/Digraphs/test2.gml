graph [
  directed 1
  node [
    id 0
    label "a"
  ]
  node [
    id 1
    label "d"
  ]
  node [
    id 2
    label "f"
  ]
  node [
    id 3
    label "e"
  ]
  node [
    id 4
    label "b"
  ]
  node [
    id 5
    label "c"
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 2
  ]
  edge [
    source 3
    target 1
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 0
  ]
  edge [
    source 4
    target 1
  ]
  edge [
    source 5
    target 1
  ]
  edge [
    source 5
    target 3
  ]
]
