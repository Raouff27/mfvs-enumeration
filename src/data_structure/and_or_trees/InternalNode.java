package data_structure.and_or_trees;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import data_structure.bdd.BDD;
import utils.StringMethods;

 abstract class InternalNode extends Node {
	public List<Node> children;

	public InternalNode(int id, String value, List<Node> children) {
		super(id, value);
		this.children = children;
	}

	public String toString(int depth) {
		String s;
		s = StringMethods.spaces(depth) + toString() + '\n';
		for (Node child : children) {
			s += child.toString(depth + 2);
		}
		return s;
	}

	/**
	 * Returns the children of this node.
	 *
	 * @return A list of the child nodes
	 */
	public List<Node> getChildren() {
		return children;
	}

	/**
	 * Returns the arity of this node, i.e the number of its children.
	 *
	 * @return The arity (number of children)
	 */
	int arity() {
		return children.size();
	}

	/**
	 * Returns the number of nodes in the subtree induced by this node.
	 *
	 * @return The number of nodes
	 */
	@Override
	int numberOfNodes() {
		int n = 1;
		for (Node child : children) {
			n += child.numberOfNodes();
		}
		return n;
	}
	public int hight(){
		return this.hight();
	}
	/**
	 * Returns the number of solutions in the subtree induced by this node.
	 *
	 * @return The number of solutions
	 */
	public int numberOfSolutions() {
		return this.numberOfSolutions();
	}

	/**
	 * Returns the values occurring in the subtree induced by this node.
	 *
	 * @return The values occurring in the subtree
	 */
	@Override
	HashSet<String> values() {
		HashSet<String> hs = children.get(0).values();
		for (Node child : children) {
			hs.addAll(child.values());
		}
		return hs;
	}

	public String toGraphViz() {
		return this.toGraphViz();
	}
	
	public ArrayList<String> idList(ArrayList<String> list) {
		return this.idList(list);
	}
	
	public Node searchNode(int id, Node m) {
		return this.searchNode(id,m);
	}
	
	public void addNode(Node m, int parent) {
		this.addNode(m, parent);
	}
	public String saveTree(boolean b, String st) {
		return this.saveTree(b,st);
	}
	
	public Node flat(Node n) {
		return this.flat(n);
	}
	
	public boolean compare(Node n, Node m, boolean b){
		return this.compare(n, m,b);
	}
	public BDD convertTreeToBDD(Node root,data_structure.bdd.BDD bdd,int id,data_structure.bdd.ExternalNode zeroTerminal,data_structure.bdd.ExternalNode unTerminal) {
	return this.convertTreeToBDD(root,bdd,id,zeroTerminal,unTerminal);
	}
	
	/*public BDD transfertToBDD(Node root,data_structure.bdd.BDD bdd,int id) {
		return this.transfertToBDD(root,bdd,id);
	}*/

	
	public ArrayList<String> randomSolution(ArrayList<String> solution) {
		return this.randomSolution(solution);
	}
	public int isUniform(int k) {
		return this.isUniform(k);
	}
	
	public Node binarize(Node root,int id) {
		return this.binarize(root,id);
	}
	
	public boolean isSolution(Set<String> solutionSet,boolean b) {
		return this.isSolution(solutionSet,b);
	}
}
