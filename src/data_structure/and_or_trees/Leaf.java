package data_structure.and_or_trees;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import data_structure.bdd.BDD;
public class Leaf extends Node{
    /**
     * 
     * @param id
     * @param value
     */
	public Leaf(int id, String value) {
		super(id, value);
	}
	/**
	 * 
	 */
	public String getValue(){
		return super.getValue();
	}
	/**
	 * 
	 */
	public String toString() {
		return getValue();
	}
	
	/**
	 * Returns the number of nodes in the subtree induced by this leaf, i.e
	 * exactly one.
	 *
	 * @return Always 1
	 */
	int numberOfNodes() {
		return 1;
	}
	/**
	 * 
	 */
	public int hight() {
		return 0;
	}
	
	/**
	 * Returns the set of values occurring in the subtree induced by this leaf,
	 * i.e exactly the singleton containing the value stored in this leaf.
	 *
	 * @return The singleton
	 */
	HashSet<String> values() {
		HashSet<String> hs = new HashSet<String>();
		hs.add(value);
		return hs;
	}

	/**
	 * Returns the number of solutions in the subtree induced by this leaf, i.e
	 * exactly one.
	 *
	 * @return Always 1
	 * */
	public int numberOfSolutions() {
		return 1;
	}

	/**
	 * return a string used to drawing the BDD with GraphViz
	 * 
	 * @return
	 */
	public String toGraphViz() {
		
		return "\n"+getId()+ " [label=" + (char) 34 + getValue()+ (char) 34+", style=filled, fillcolor=none];";
	}

	public ArrayList<String> idList(ArrayList<String> list) {
		list.add(""+this.getId());
		return list;
	}
	
	public Node searchNode(int id, Node m) {
		if(this.getId()==id){
		 m= this;	
		}
		return m;
	}
	
	public void addNode(Node m, int parent) {
	   return;
	}
	
	public String saveTree(boolean b,String st) {
		return st;
	}
	
	public Node flat(Node n) {
		return this;
	}
	
	public boolean compare(Node n, Node m,boolean b){
		b= n.getValue().equals(m.getValue());
		return b;
	}
	
	
	public BDD convertTreeToBDD(Node root,data_structure.bdd.BDD bdd,int id,data_structure.bdd.ExternalNode zeroTerminal,data_structure.bdd.ExternalNode unTerminal) {
		bdd = new BDD(id, this.getValue(),false,zeroTerminal, unTerminal);
		//id=id+1;
		return bdd;
	}

    /**
     * 	
     */
	public ArrayList<String> randomSolution(ArrayList<String> solution) {
		solution.add(this.getValue());
		return solution;
	}
	/***
	 * 
	 ****/
	public int isUniform(int k) {
		return 1;
	}
	
	/**
	 * 
	 */
	public Node binarize(Node root,int id) {
		id=id+1;
		return new Leaf(id, this.getValue());
	}
	public Set<String>  isSolution(Set<String> solutionSet,Set<String> result) {
		if(solutionSet.containsAll(this.values())){ 
			System.out.println(this.getValue());
			result.add(this.getValue());
		}
		return result;
	}
}
