package data_structure.and_or_trees;
import java.util.Iterator;
import java.util.ArrayList;

class AndNodeIterator extends NodeIterator {

    // Attributes
    private AndNode node;
    private ArrayList<Solution> currentSolution;
    private ArrayList<Iterator<Solution>> iterators;
    private boolean done;

    /**
     * Constructor.
     *
     * @param node  The and-node on which we iterate
     */
    AndNodeIterator(AndNode node) {
        this.node = node;
        this.iterators = new ArrayList<Iterator<Solution>>();
        this.done = false;
        for (Node child: node.getChildren()) {
            if (!child.iterator().hasNext()) {
                this.done = true;
            }
            this.iterators.add(child.iterator());
        }
        if (!this.done) {
            this.currentSolution = new ArrayList<Solution>();
            for (int i = 0; i < iterators.size() - 1; ++i) {
                this.currentSolution.add(null);
                setNextValue(i);
            }
            this.currentSolution.add(null);
        }
    }
    
    /**
     * Indicates whether the value has been returned yet.
     *
     * @return  True if and only if the value has not been
     *          returned
     */
    @Override
    public boolean hasNext() {
        if (!done) {
            for (Iterator<Solution> it: iterators) {
                if (it.hasNext()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns the next solution for this node.
     *
     * @return  The next solution
     */
    @Override
    public Solution next() {
        int i = iterators.size() - 1;
        while (i >= 0 && !iterators.get(i).hasNext())
            --i;
        for (int j = i + 1; j < iterators.size(); ++j) {
            iterators.set(j, node.getChildren().get(j).iterator());
        }
        for (int j = i; j < iterators.size(); ++j) {
            setNextValue(j);
        }
        return current();
    }

    /**
     * Returns the current solution.
     *
     * @return  The solution to be returned when next is called
     */
    private Solution current() {
        Solution solution = new Solution();
        for (Solution partial: currentSolution) {
            solution.addAll(partial);
        }
        return solution;
    }

    /**
     * Updates the current solution according to the next
     * element in i-th iterator.
     *
     * @param i  The index of the iterator to updated
     */
    private void setNextValue(int i) {
        this.currentSolution.set(i, iterators.get(i).next());
    }

    /**
     * Unsupported operation.
     *
     * Needed so that it compiles everywhere.
     */
	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

}
