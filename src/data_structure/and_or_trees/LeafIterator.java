package data_structure.and_or_trees;



import java.util.NoSuchElementException;

class LeafIterator extends NodeIterator {

    // Attributes
    String value;
    boolean done;

    /**
     * Constructor.
     *
     * @param leaf  The leaf on which we iterate
     */
    LeafIterator(Leaf leaf) {
        this.value = leaf.getValue();
        this.done = false;
    }

    /**
     * Indicates whether the value has been returned yet.
     *
     * @return  True if and only if the value has not been
     *          returned
     */
    public boolean hasNext() {
        return !done;
    }

    /**
     * Returns the singleton formed by the value in this leaf.
     *
     * @return  A singleton containing the value of this leaf
     */
    public Solution next() {
        if (!done) {
            done = true;
            return new Solution(value);
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * Unsupported operation.
     *
     * Needed so that it compiles everywhere.
     */
	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
