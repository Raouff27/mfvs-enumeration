package data_structure.and_or_trees;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import data_structure.bdd.BDD;
import utils.StringMethods;

public class Node implements Iterable<Solution> {
	private int id;
	protected String value;

	public Node(int id, String value) {
		this.id = id;
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	String toString(int depth) {
		return StringMethods.spaces(depth) + toString() + '\n';
	}

	/**
	 * Returns the number of nodes in the subtree induced by this node.
	 *
	 * @return The number of nodes
	 */
	int numberOfNodes() {
		return numberOfNodes();
	}

	/**
	 * Returns the number of solutions in the subtree induced by this node.
	 *
	 * @return The number of nodes
	 */
	int numberOfSolutions() {
		return numberOfSolutions();
	}

	/**
	 * Returns the set of values occurring in the subtree induced by this node.
	 *
	 * @return The set of values
	 */
	HashSet<String> values() {
		return this.values();
	}

	@Override
	public Iterator<Solution> iterator() {
		if (this instanceof AndNode) {
			return new AndNodeIterator((AndNode) this);
		} else

		if (this instanceof OrNode) {
			return new OrNodeIterator((OrNode) this);
		} else {
			return new LeafIterator((Leaf) this);
		}

	}

	public String toGraphViz() {
		return this.toGraphViz();
	}

	public int hight() {
		return this.hight();
	}

	public Node flat(Node n) {
		return this.flat(n);
	}

	public ArrayList<String> idList(ArrayList<String> list) {
		return this.idList(list);
	}

	public Node searchNode(int id, Node m) {
		return this.searchNode(id,m);
	}

	public void addNode(Node m, int parent) {
		addNode(m, parent);
	}

	public String saveTree(boolean b, String st) {
		return this.saveTree(b,st);
	}
	
	public boolean compare(Node n, Node m, boolean b){
		return compare(n, m, b);
	}

	public BDD convertTreeToBDD(Node root,data_structure.bdd.BDD bdd,int id,data_structure.bdd.ExternalNode zeroTerminal,data_structure.bdd.ExternalNode unTerminal) {
		return this.convertTreeToBDD(root,bdd,1,zeroTerminal,unTerminal);
	}
	
	
	public ArrayList<String> randomSolution(ArrayList<String> solution) {
		return this.randomSolution(solution);
	}

	public int isUniform(int k) {
		return this.isUniform(k);
	}

	public Node binarize(Node root, int id) {
		return this.binarize(root,id);
	}

	public Set<String> isSolution(Set<String> solutionSet,Set<String> result) {
		return this.isSolution(solutionSet,result);
	}
	

}
