package data_structure.and_or_trees;
import java.util.ArrayList;

public class Solution extends ArrayList<String> {

    // Attribute
    private static final long serialVersionUID = 1;

    /**
     * Creates an empty solution.
     */
    Solution() {
        super();
    }

    /**
     * Creates a solution with given value.
     *
     * @param value  The value to store in the solution
     */
    Solution(String value) {
        super();
        add(value);
    }

}
