package data_structure.and_or_trees;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import data_structure.bdd.BDD;

public class OrNode extends InternalNode {
    /**
     * 
     * @param id
     * @param value
     * @param children
     */
	public OrNode(int id, String value, List<Node> children) {
		super(id, "OR", children);

	}
    /**
     * 
     */
	public String toString() {
		return "OR";
	}

	/**
	 * Returns an iterator over the solutions represented by the subtree induced
	 * by this node.
	 *
	 * @return The iterator
	 */
	@Override
	public NodeIterator iterator() {
		return new OrNodeIterator(this);
	}

	/**
	 * Returns the number of solutions in the subtree induced by this node.
	 *
	 * @return The number of solutions
	 */
	public int numberOfSolutions() {
		int i = 0;
		for (Node child : children) {
			i += child.numberOfSolutions();
		}
		return i;
	}
    /**
     * 
     */
	public int hight() {
		int i = 0;
		for (Node child : children) {
			i = Math.max(child.hight(), i);
		}
		return i + 1;
	}
    /**
     * 
     */
	public String toGraphViz() {
		String st = "";

		st = "\n" + getId() + " [label=" + (char) 34 + getValue() + (char) 34
				+ ", style=filled, fillcolor=coral];";

		for (Node child : children) {
			st += child.toGraphViz() + "\n" + getId() + " -> " + child.getId()
					+ ";";
		}
		return st;
	}

	public ArrayList<String> idList(ArrayList<String> list) {
		list.add("" + this.getId());
		for (Node node : children) {
			list = node.idList(list);
		}
		return list;
	}

	public Node searchNode(int id, Node m) {
		if (this.getId() == id) {
			m = this;
		} else {
			for (Node node : children)
				m = node.searchNode(id, m);
		}
		return m;
	}
	


	public void addNode(Node m, int parent) {
		if (this.getId() == parent) {
			boolean b=false;int i=0;
			while(i<children.size() &&  !b){
				Node node=children.get(i);
				if (node==m) b=true;
				i++;
			}
			if (!b) this.children.add(m);
		} else {
			for (Node node : children) {
				node.addNode(m, parent);
			}
		}
	}

	public String saveTree(boolean b, String st) {
		if (!b) {
			st = this.getId() + ":" + this.getValue() + ":-1\n";
			b = true;
		}
		for (Node node : children) {
			st = st + node.getId() + ":" + node.getValue() + ":" + this.getId()
					+ "\n";
			st = node.saveTree(b, st);
		}
		return st;
	}

	public Node flat(Node n) {
		if(this.children.size()==0){
			n=null;
		}else{
		if (this.children.size() == 1) {
			n = children.get(0).flat(null);
		} else {
			if(this.children.size()==0){
				n=null;
			}else{
			n = new OrNode(this.getId(), "OR", new ArrayList<Node>());
				for (Node node : children) {
					if(node instanceof AndNode){
						if (((AndNode)node).children.size()!=0){
							Node m = node.flat(null);
							n.addNode(m, n.getId());	
						}
					}else{
						if(node instanceof Leaf){
							Node m = node.flat(null); 
							n.addNode(m, n.getId());
						}else{
							for (Node node2:((OrNode)node).children){
							Node m = node2.flat(null);
							n.addNode(m, n.getId());
							}
						}
						
					}
				}
		}
		}
		}
		return n;
	}
	
	public boolean compare(Node n, Node m,boolean b){
		if(n.getValue().equals(m.getValue())){
			for(Node node1:((OrNode)n).children){
				for (Node node2:((OrNode)n).children){
					b=compare(node1,node2,b);
				}
			}
		}else{
			b= false;
		}
		return b;
	}

	public BDD convertTreeToBDD(Node root,data_structure.bdd.BDD bdd,int id,data_structure.bdd.ExternalNode zeroTerminal,data_structure.bdd.ExternalNode unTerminal) {
		int size = ((InternalNode)root).children.size()-1;
		Node n= ((InternalNode)root).children.get(size);
		data_structure.bdd.BDD bddlow = (n.convertTreeToBDD(n, null, id,zeroTerminal, unTerminal));
		id= bddlow.getRoot().getId()+1;
	    for(int i =(size-1); i>=0 ;i--){
			data_structure.bdd.BDD bddhigh = ((InternalNode)root).children.get(i).convertTreeToBDD(((InternalNode)root).children.get(i), null, id,zeroTerminal, unTerminal);
	        BDD bd = new BDD();
	        id=bddhigh.getRoot().getId()+1;
	        bdd= bd.bddOperation(bd.operation(bddhigh.getRoot(),bddlow.getRoot(), "OR", null,zeroTerminal,unTerminal));
	        bddlow = bdd;
			id= bdd.getRoot().getId()+1;
		}
			return bdd;
	}

	/**

	 */
	public ArrayList<String> randomSolution(ArrayList<String> solution) {
		Random dice = new Random();
		int random = dice.nextInt(this.numberOfSolutions());
		int k = 0; int i=0; boolean b=false;
		while(i<children.size() && !b){
			Node node = children.get(i);
			if(random>=k && random < k + node.numberOfSolutions()){
				solution = node.randomSolution(solution);
				b=true;
			}else{
				k=k+node.numberOfSolutions();	
			}
			i++;
		}
		return solution;
	}
	
	public int isUniform(int k) {
		if (k != -1) {
			int l = children.get(0).isUniform(0);
			int i = 1;
			boolean b = true;
			while (i < children.size() && b) {
				int m = children.get(i).isUniform(0);
				if (m == l) {
					i++;
				} else {
					k = -1;
					b = false;
				}
			}
			if (b)
				k = l;
		}
		return k;
	}
	
	public Node binarize(Node root,int id) {
		if (this.children.size() == 2) {
			ArrayList<Node> childd = new ArrayList<Node>();
			Node n1 = this.children.get(0).binarize(null, id);
			id = id+n1.numberOfNodes();
			Node n2 = this.children.get(1).binarize(null, id);
			childd.add(n1);
			childd.add(n2);
			id = id + n2.numberOfNodes()+1;
			root = new OrNode(id, "OR", childd);
            id++;
		} else {
			int i=children.size()-1;
			Node n1 = this.children.get(i-1).binarize(null,id);
			id=id+n1.numberOfNodes();
			i--;
			Node n2 = this.children.get(i+1).binarize(null,id);
			ArrayList<Node> childs = new ArrayList<Node>();
			childs.add(n1);
			childs.add(n2);
			id=id+n2.numberOfNodes()+1;
			i--;
			Node node = new OrNode(id,"OR",childs);
			while(i>=0){
				Node n = children.get(i).binarize(null,id);
				id=id+n.numberOfNodes()+1;
				ArrayList<Node> childs2 = new ArrayList<Node>();
				childs2.add(n);
				childs2.add(node);
				node = new OrNode(id,"OR",childs2);
				i--;
			}
			root =node;
		}
		return root;
	}
	
	public Set<String> isSolution(Set<String> solutionSet,Set<String> result) {
		Set<String> result2 =result;
		int i=0; boolean ok=false;
		while (i<children.size() && !ok){
			result = children.get(i).isSolution(solutionSet, result);
			
			if(result.equals(result2)){
				ok=false;
			}else{
				ok=true;
			}
		   i++;
		}
		return result;
	}
}
