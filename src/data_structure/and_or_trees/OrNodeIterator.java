package data_structure.and_or_trees;
import java.util.LinkedList;

class OrNodeIterator extends NodeIterator {

    // Attributes
    OrNode node;
    LinkedList<NodeIterator> iterators;

    /**
     * Constructor.
     *
     * @param node  The or-node on which we iterate.
     */
    OrNodeIterator(OrNode node) {
        this.node = node;
        this.iterators = new LinkedList<NodeIterator>();
        for (Node child: node.getChildren()) {
            this.iterators.add((NodeIterator) child.iterator());
        }
    }

    /**
     * Indicates whether the value has been returned yet.
     *
     * @return  True if and only if the value has not been
     *          returned
     */
    @Override
    public boolean hasNext() {
        return !iterators.isEmpty();
    }

    /**
     * Returns the next solution for this node.
     *
     * @return  The next solution
     */
    @Override
    public Solution next() {
        Solution solution = iterators.element().next();
        if (!iterators.element().hasNext()) {
            iterators.remove();
        }
        return solution;
    }

    /**
     * Unsupported operation.
     *
     * Needed so that it compiles everywhere.
     */
	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

}
