package data_structure.and_or_trees;

import java.util.ArrayList;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import data_structure.bdd.BDD;

public class AndOrTree implements Iterable<Solution> {
	Node root = null;
    /**
     * 
     */
	public AndOrTree() {
		root = null;
	}
    /**
     * 
     * @param id
     * @param value
     */
	public AndOrTree(int id, String value) {
		root = new Leaf(id, value);
	}
    /**
     * 
     * @param id
     * @param value
     * @param children
     */
	public AndOrTree(int id, String value, List<Node> children) {
		if (value == "AND") {
			root = new AndNode(id, "AND", children);
		} else if (value == "OR") {
			root = new OrNode(id, "OR", children);
		} else {
			throw new IllegalArgumentException(
					"Node type must be either 'AND' or 'OR'.");
		}
	}
	/**
     * 
     */
	public String toString() {
		return root.toString(0);
	}
    /**
     * 
     * @return
     */
	public Node getRoot() {
		return root;
	}
    /**
     * 
     * @param root
     */
	public void setRoot(Node root) {
		this.root = root;
	}

	/**
	 * Returns an iterator over the solutions represented by this tree.
	 *
	 * @return The iterator
	 */
	@Override
	public Iterator<Solution> iterator() {
		return root.iterator();
	}

	/**
	 * Returns a list of all solutions represented by this tree.
	 *
	 * @return The list
	 */
	public ArrayList<Solution> getSolutions(AndOrTree tree) {
		ArrayList<Solution> listSolutions = new ArrayList<Solution>();
		for (Solution solution : tree) {
			listSolutions.add(solution);
		}
		return listSolutions;
	}

	/**
	 * Returns the number of nodes in the tree.
	 *
	 * @return The number of nodes
	 */
	public HashSet<String> values() {
		return getRoot().values();
	}

	/**
	 * Returns the number of nodes in the tree.
	 *
	 * @return The number of nodes
	 */
	public int numberOfNodes() {
		return root.numberOfNodes();
	}

	/**
	 * Returns the hight of the tree.
	 *
	 * @return The hight
	 */
	public int hight() {
		return root.hight();
	}

	/**
	 * Returns the number of solutions represented by this tree.
	 * 
	 * @return
	 */
	public int numberOfSolutions() {
		return root.numberOfSolutions();
	}
	/**
	 * 
	 * @return
	 */
	public ArrayList<String> randomSolution() {
		return root.randomSolution(new ArrayList<String>());
	}
	/**
	 * 
	 * @param k
	 * @return
	 */
	public int isUniform(int k) {
		return root.isUniform(k);
	}
    /**
     * 
     * @return
     */
	public String toGraphViz() {
		return "digraph {" + root.toGraphViz() + "\n}";
	}
    /**
     * 
     * @param id
     * @param m
     * @return
     */
	public Node searchNode(int id, Node m) {
		return root.searchNode(id, m);
	}

	/**
	 * 
	 * @param list
	 * @return
	 */
	public ArrayList<String> idList(ArrayList<String> list) {
		return root.idList(list);
	}
    /**
     * 
     * @param m
     * @param parent
     */
	public void addNode(Node m, int parent) {
		root.addNode(m, parent);
	}
     /**
     * 
     * @return
     */
	public String saveTree() {
		return root.saveTree(false, "");

	}
    /**
     * 
     * @return
     */
	public AndOrTree flat() {
		Node n = root.flat(null);
		
		if (n != null) {
			if (n instanceof AndNode) {
				return new AndOrTree(n.getId(), n.getValue(),
						((AndNode) n).children);
			} else {
				if (n instanceof OrNode) {
				     return new AndOrTree(n.getId(), n.getValue(),
							((OrNode) n).children);
				} else {
					return new AndOrTree(n.getId(), n.getValue());
				}
			}
		} else {
			return null;
		}
	}

	/**
	 * 
	 * @return
	 */
	public BDD convertTreeToBDD(){
		data_structure.bdd.ExternalNode zeroTerminal =new data_structure.bdd.ExternalNode(0,"0",false);
		data_structure.bdd.ExternalNode unTerminal =new data_structure.bdd.ExternalNode(1,"1",false);

		return root.convertTreeToBDD(root,null,2,zeroTerminal,unTerminal);
	}
    /**
     * 
     * @return
     */
	public Node binarize() {
		return root.binarize(root, 0);
	}
    /**
     * 
     * @param solutionSet
     * @param result
     * @return
     */
	public Set<String> isSolution(Set<String> solutionSet, Set<String> result) {
		return root.isSolution(solutionSet, result);
	}

}