package data_structure.bdd;
import data_structure.and_or_trees.AndOrTree;
import utils.StringMethods;
public class ExternalNode extends Node {
	/**
	 * Constructor
	 * 
	 * @param id
	 * @param val
	 * @param mark
	 */
	public ExternalNode(int id, String var, boolean mark) {
		super(id, var, mark);
	}
	
	/**
	 * mark this node on false
	 */
	public void setMarkFalse() {
		this.mark = false;
	}
	/**
	 * mark this node on true
	 */
	public void setMarkTrue() {
		this.mark = true;
	}
	
	public int numberOfNodes() {
		int i =0;
		if(this.getMark()==false){
			this.mark=true;
			i= 1;
		}
		return i;
		
	}
	
	public int hight() {
		return 0;
	}

	/**
	 * Returns a string representation of this node.
	 *
	 * @depth The number of leading empty space in the string
	 * @return The string representation of the node
	 */
	public String toString(int depth) {
		return StringMethods.spaces(depth) + getValue() + "\n";
	}
   
	/**
	 * return a string used to drawing the BDD with GraphViz
	 * 
	 * @return
	 */
	public String toGraphViz() {
		if (getMark() == false) {
			setMarkTrue();
			return " \n" + getId() + " [label=" + getValue()
					+ ", shape=box, regular=1, fixedsize=true,width=0.3];";
		} else {
			return "";
		}
	}
	
	public Node searchNode(int id, Node m) {
		if(this.getMark()==false){
			this.setMarkTrue();
		if(this.getId()==id){
		 m= this;	
		}
		}
		return m;
	}
	
	public AndOrTree transfertToTree(Node root,data_structure.and_or_trees.AndOrTree tree,int id) {
		if (this.getValue().equals("0")) {
			tree = new AndOrTree(id, "0-terminal");
		} else {
			tree = new AndOrTree(id, "1-terminal");
		}
		return tree;
	}
}