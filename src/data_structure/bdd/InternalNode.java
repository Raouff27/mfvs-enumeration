package data_structure.bdd;
import java.util.ArrayList;

import data_structure.and_or_trees.AndOrTree;
import utils.StringMethods;

public class InternalNode extends Node {
	int lab = 100;
	/**
	 * Attributes
	 */
	Node low;
	Node high;

	/**
	 * Constructor
	 * 
	 * @param id
	 * @param val
	 * @param mark
	 * @param low
	 * @param high
	 */
	public InternalNode(int id, String var, boolean mark, Node low, Node high) {
		super(id, var, mark);
		this.low = low;
		this.high = high;
	}

	/**
	 * 
	 * @return the low child
	 */
	public Node getLow() {
		return low;
	}
	/**
	 * 
	 * @return the high child
	 */
	public Node getHigh() {
		return high;
	}

	public void setLow(Node node) {
		this.low = node;
	}

	public void setHigh(Node node) {
		this.high = node;
	}

	public int numberOfNodes() {
		int i = 0;
		if (this.getMark() == false) {
			this.mark = true;
			i = 1 + low.numberOfNodes() + high.numberOfNodes();
		}
		return i;
	}

	public int hight() {
		return 1 + Math.max(this.low.hight(), this.high.hight());
	}

	/**
	 * mark this node on false
	 */
	public void setMarkFalse() {
		this.mark = false;
		low.setMarkFalse();
		high.setMarkFalse();
	}

	/**
	 * Returns a string representation of this node.
	 *
	 * @depth The number of leading empty space in the string
	 * @return The string representation of the node
	 */
	public String toString(int depth) {

		String s;
		s = StringMethods.spaces(depth) + getValue() + '\n';
		s += low.toString(depth + 2) + " " + high.toString(depth + 1);

		return s;
	}

	/**
	 * return a string used to drawing the BDD with GraphViz
	 * 
	 * @return
	 */
	public String toGraphViz() {

		if (getMark() == false) {
			setMarkTrue();
			return "\n" + getId() + " [label=" + getValue()
					+ ", style=filled, fillcolor=none]; \n"    //  ", shape=circle, fixedsize=true, hight=0.3]; \n"
					+ getId() + "->" + low.getId()
					+ " [style=dashed,  arrowsize=.5]; \n" + getId() + "->"
					+ high.getId() + " [arrowsize=0.5];" + low.toGraphViz()
					+ high.toGraphViz();
		} else {
			return "";
		}
	}
	
	
	public Node searchNode(int id, Node m) {
		if(this.getMark()==false){
			this.setMarkTrue();
		if (this.getId() == id) {
			m = this;
		} else {
			m = this.getLow().searchNode(id, m);
			m = this.getHigh().searchNode(id, m);
		}
		}
		return m;
	}
	
	public AndOrTree transfertToTree(Node root,	data_structure.and_or_trees.AndOrTree tree, int id) {
		Node high = ((InternalNode) root).getHigh();
		Node low = ((InternalNode) root).getLow();
		if ((high instanceof ExternalNode)&& (low instanceof ExternalNode)){
			tree = new AndOrTree(id, root.getValue());	
		}else{
			if ((high instanceof InternalNode)&& (low instanceof ExternalNode)){
				ArrayList<data_structure.and_or_trees.Node> children = new ArrayList<data_structure.and_or_trees.Node>();
				children.add((new AndOrTree(id, root.getValue())).getRoot());
				id=id+1;
				data_structure.and_or_trees.AndOrTree highTree = high.transfertToTree(high, tree, id);
				children.add(highTree.getRoot());
				id = highTree.getRoot().getId() + 1;
				tree = new AndOrTree(id, "AND", children);
			}else{
				if ((low instanceof InternalNode)&& (high instanceof ExternalNode)){
					ArrayList<data_structure.and_or_trees.Node> children = new ArrayList<data_structure.and_or_trees.Node>();
					children.add((new AndOrTree(id, root.getValue())).getRoot());
					id=id+1;
					data_structure.and_or_trees.AndOrTree lowTree = low.transfertToTree(low, tree, id);
					children.add(lowTree.getRoot());
					id = lowTree.getRoot().getId() + 1;
					tree = new AndOrTree(id, "OR", children);
				}else{
					
					
					ArrayList<data_structure.and_or_trees.Node> children1 = new ArrayList<data_structure.and_or_trees.Node>();
					children1.add((new AndOrTree(id, root.getValue())).getRoot());
					id=id+1;
					data_structure.and_or_trees.AndOrTree highTree = high.transfertToTree(high, tree, id);
					children1.add(highTree.getRoot());
					id = highTree.getRoot().getId() + 1;
					data_structure.and_or_trees.AndOrTree tree1 = new AndOrTree(id, "AND", children1);
					
					ArrayList<data_structure.and_or_trees.Node> children2 = new ArrayList<data_structure.and_or_trees.Node>();
					children2.add(tree1.getRoot());
					id=id+1;
					data_structure.and_or_trees.AndOrTree lowTree = low.transfertToTree(low, tree, id);
					children2.add(lowTree.getRoot());
					id = lowTree.getRoot().getId() + 1;
					tree = new AndOrTree(id, "OR", children2);
				}
			}

		}
	
		/*data_structure.and_or_trees.AndOrTree highTree = 
				(((InternalNode) root).getHigh()).transfertToTree((((InternalNode) root).getHigh()),tree, id);
		data_structure.and_or_trees.AndOrTree lowTree = 
				(((InternalNode)root).getLow()).transfertToTree((((InternalNode)root).getLow()), tree, id);
		
		if (highTree.getRoot().getValue().equals("1-terminal") && lowTree.getRoot().getValue().equals("0-terminal")){
			tree = new AndOrTree(id, root.getValue());	
		}else{
			if ((highTree.getRoot().getValue() != "1-terminal") && lowTree.getRoot().getValue().equals("0-terminal")){
				ArrayList<data_structure.and_or_trees.Node> children = new ArrayList<data_structure.and_or_trees.Node>();
				id=id+1;
				children.add((new AndOrTree(id, root.getValue())).getRoot());
				children.add(highTree.getRoot());
				id = id + 1;
				tree = new AndOrTree(id, "AND", children);
				id = id + 1;
			}else{
				if(((lowTree.getRoot().getValue() != "1-terminal") && highTree.getRoot().getValue().equals("1-terminal"))){
					ArrayList<data_structure.and_or_trees.Node> children = new ArrayList<data_structure.and_or_trees.Node>();
					id=id+1;
					children.add((new AndOrTree(id, root.getValue())).getRoot());
					children.add(lowTree.getRoot());
					id = id+ 1;
					tree = new AndOrTree(id, "OR", children);
					id = id + 1;	
				}else{
					ArrayList<data_structure.and_or_trees.Node> children1 = new ArrayList<data_structure.and_or_trees.Node>();
					id=id+1;
					children1.add((new AndOrTree(id, root.getValue())).getRoot());
					children1.add(highTree.getRoot());
					id = id + 1;
					data_structure.and_or_trees.AndOrTree tree2 = new AndOrTree(id, "AND", children1);
					ArrayList<data_structure.and_or_trees.Node> children2 = new ArrayList<data_structure.and_or_trees.Node>();
					id=id+1;
					children2.add(tree2.getRoot());
					children2.add(lowTree.getRoot());
					id = id+ 1;
					tree = new AndOrTree(id, "OR", children2);
					id = id + 1;
				}
			}

		}
		
		
	/*	
		
		
		if (highTree.getRoot().getValue().equals("1-terminal")) {
			tree = new AndOrTree(id, root.getValue());
			id = id + 1;
		} else {
			id = id + 1;
			ArrayList<data_structure.and_or_trees.Node> children = new ArrayList<data_structure.and_or_trees.Node>();
			children.add((new AndOrTree(id, root.getValue())).getRoot());
			children.add(highTree.getRoot());
			id = id + 1;
			tree = new AndOrTree(id, "AND", children);
			id = id + 1;

		}
		
		//System.out.println(id +"  "+tree.numberOfNodes());
		if (lowTree.getRoot().getValue() != "1-terminal" && lowTree.getRoot().getValue() != "0-terminal") {
			ArrayList<data_structure.and_or_trees.Node> children2 = new ArrayList<data_structure.and_or_trees.Node>();
			children2.add(tree.getRoot());
			children2.add(lowTree.getRoot());
			
			id = id+ lowTree.numberOfNodes();
			tree = new AndOrTree(id, "OR", children2);
			id = id + 1;
		}
		*/
		return tree;
	}
	
	
	/*public AndOrTree transfertToTree(Node root,	data_structure.and_or_trees.AndOrTree tree, int id) {
		data_structure.and_or_trees.AndOrTree highTree = 
				(((InternalNode) root).getHigh()).transfertToTree((((InternalNode) root).getHigh()),tree, id);
				
		if (highTree.getRoot().getValue().equals("1-terminal")) {
			tree = new AndOrTree(id, root.getValue());
			id = id + 1;
		} else {
			id = id + 1;
			ArrayList<data_structure.and_or_trees.Node> children = new ArrayList<data_structure.and_or_trees.Node>();
			children.add((new AndOrTree(id, root.getValue())).getRoot());
			children.add(highTree.getRoot());
			id = id + 1;
			tree = new AndOrTree(id, "AND", children);
			id = id + 1;

		}
		data_structure.and_or_trees.AndOrTree lowTree = 
				(((InternalNode)root).getLow()).transfertToTree((((InternalNode)root).getLow()), tree, id);

		//System.out.println(id +"  "+tree.numberOfNodes());
		if (lowTree.getRoot().getValue() != "1-terminal" && lowTree.getRoot().getValue() != "0-terminal") {
			ArrayList<data_structure.and_or_trees.Node> children2 = new ArrayList<data_structure.and_or_trees.Node>();
			children2.add(tree.getRoot());
			children2.add(lowTree.getRoot());
			
			id = id+ lowTree.numberOfNodes();
			tree = new AndOrTree(id, "OR", children2);
			id = id + 1;
		}
		
		return tree;
	}*/
}