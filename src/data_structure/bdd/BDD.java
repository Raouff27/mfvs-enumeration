package data_structure.bdd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import data_structure.and_or_trees.AndOrTree;

public class BDD {
	private Node root = null;
	

	/**
	 * Creates an empty BDD.
	 */
	public BDD() {
		root = null;
	}

	/**
	 * Creates a BDD of one node.
	 * 
	 * @param id
	 * @param val
	 * @param mark
	 */
	public BDD(int id, String var, boolean mark) {
		root = new ExternalNode(id, var, mark);
	}

	/**
	 * Creates an BDD with given root node and children.
	 * 
	 * @param id
	 * @param val
	 * @param mark
	 * @param low
	 * @param high
	 */
	public BDD(int id, String var, boolean mark, Node low, Node high) {
		root = new InternalNode(id, var, mark, low, high);
	}

	/**
	 * Returns the root of this BDD.
	 *
	 * @return The root
	 */
	public Node getRoot() {
		return root;
	}

	/**
	 * Returns the number of nodes in the tree.
	 *
	 * @return The number of nodes
	 */
	public int numberOfNodes() {
		return root.numberOfNodes();
	}

	public int hight() {
		return root.hight();
	}

	/**
	 * Returns a string representation of this BDD.
	 *
	 * @return A string representation of this BDD
	 */
	public String toString() {
		return root.toString(0);
	}

	/**
	 * return a string used to drawing the BDD with GraphViz
	 * 
	 * @return
	 */
	public String toGraphViz() {
		return "digraph bdd {" + root.toGraphViz() + "\n}";
	}

	/**
	 * 
	 * @param n
	 *            the root
	 * @return
	 */
	public String saveBDD(Node n) {
		String st = "";
		if (n.getMark() == false) {
			n.setMarkTrue();
			if (n instanceof InternalNode) {
				st = st + n.getId() + ":" + n.getValue() + ":"
						+ ((InternalNode) n).getLow().getId() + ":"
						+ ((InternalNode) n).getHigh().getId() + "\n";
				st = st + saveBDD(((InternalNode) n).getLow());
				st = st + saveBDD(((InternalNode) n).getHigh());
			} else {
				st = st + n.getId() + ":" + n.getValue() + ":-1:-1\n";
			}
		}
		return st;
	}

	/**
	 * search and return a node
	 * 
	 * @param id
	 *            : node identifier
	 * @param m
	 *            : the node
	 * @return a node
	 */
	public Node searchNode(int id, Node m) {
		return root.searchNode(id, m);
	}

	/**
	 * set false to mark
	 * 
	 * @param n
	 *            : root of the BDD
	 */
	public void setMarkFalse(Node n) {
		if (n instanceof InternalNode) {
			n.setMarkFalse();
			setMarkFalse(((InternalNode) n).getLow());
			setMarkFalse(((InternalNode) n).getHigh());
		} else
			n.setMarkFalse();
	}

	
/***********************************************************************************************
 * 	debut reduction
 ********************************************************************************************/
	
	/**
	 * 
	 * @param n
	 * @param var
	 * @return
	 */
	public int numberOfVariables(Node n, ArrayList<String> var) {
		if (n instanceof InternalNode) {
			if (n.getMark() == false) {
				boolean b = false;
				int i = 0;
				while (!b && i < var.size()) {
					if (var.get(i).equals(n.getValue())) {
						b = true;
					} else {
						i++;
					}
				}
				if (!b) {
					var.add(n.getValue());
					n.setMarkTrue();
					numberOfVariables(((InternalNode) n).getLow(), var);
					numberOfVariables(((InternalNode) n).getHigh(), var);
				}
			}
		}
		return var.size();
	}

	/**
	 * 
	 * @param root
	 * @param level
	 * @return
	 */
	public List<ArrayList<Integer>> levelOrderBottomUp(Node root,
			List<ArrayList<Integer>> level) {
		if (root instanceof InternalNode) {
			if (root.getMark() == false) {
				root.setMarkTrue();
				ArrayList<Integer> tmp = new ArrayList<Integer>();
				tmp.add(root.getId());
				tmp.add(root.hight());
				level.add(tmp);
				level = (List<ArrayList<Integer>>) levelOrderBottomUp(
						((InternalNode) root).getLow(), level);
				level = (List<ArrayList<Integer>>) levelOrderBottomUp(
						((InternalNode) root).getHigh(), level);
			}
		} else {
			if (root.getMark() == false) {
				ArrayList<Integer> tmp = new ArrayList<Integer>();
				tmp.add(root.getId());
				tmp.add(0);
				level.add(tmp);
				root.setMarkTrue();
			}
		}
		return level;
	}

	/**
	 * 
	 * @param root
	 * @return
	 */
	public List<ArrayList<Integer>> levelOrder(Node root) {
		List<ArrayList<Integer>> level = levelOrderBottomUp(root,
				new ArrayList<ArrayList<Integer>>());
		List<ArrayList<Integer>> levels = new ArrayList<ArrayList<Integer>>();
		for (int i = 0; i <= root.hight(); i++) {
			ArrayList<Integer> tmp = new ArrayList<Integer>();
			for (ArrayList<Integer> list : level) {
				if (i == list.get(1)) {
					tmp.add(list.get(0));
				}
			}
			levels.add(tmp);
		}
		return levels;
	}

	/**
	 * 
	 * @param root
	 * @param levels
	 * @param lab
	 * @return
	 */
	public List<ArrayList<Integer>> reductBDD(Node root, List<ArrayList<Integer>> levels, int lab) {
		List<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
		lab = 1;
		
		for (ArrayList<Integer> list : levels) {
			for (int i : list) {
				setMarkFalse(root);
				Node n = searchNode(i, null);
				if (n instanceof InternalNode) {
					int lowid = ((InternalNode) n).getLow().getId();
					int highid = ((InternalNode) n).getHigh().getId();
					int lowkey = 0;
					int highkey = 0;
					ArrayList<Integer> tmp = new ArrayList<Integer>();
					tmp.add(i);
					for (ArrayList<Integer> res : result) {
						if (res.get(0) == lowid) lowkey = res.get(3);
						if (res.get(0) == highid) highkey = res.get(3);
					}
        			
					if ((lowkey == highkey)) {
						tmp.add(lowkey);
						tmp.add(highkey);
						tmp.add(lowkey);
					} else {
						int j = 0;
						int lab2 = 0;
						boolean b = false;
						while ((j < result.size()) && (!b)) {
							ArrayList<Integer> keys = result.get(j);
							if (lowkey == keys.get(1) && highkey == keys.get(2)) {
								setMarkFalse(getRoot());
								Node m = searchNode(keys.get(0), null);
								if(m.getValue().equals(n.getValue())){
								b = true;
								lab2 = keys.get(3);
								}
							}
							j++;
						}
						
						if (b) {
							tmp.add(lowkey);
							tmp.add(highkey);
							tmp.add(lab2);
						}else{
								lab = lab + 1;
								tmp.add(lowkey);
								tmp.add(highkey);
								tmp.add(lab);	
							}
						
					}
					result.add(tmp);
				} else {
					if (n.getValue().equals("0")) {
						ArrayList<Integer> tmp = new ArrayList<Integer>();
						tmp.add(i);
						tmp.add(0);
						tmp.add(-1);
						tmp.add(0);
						result.add(tmp);
					} else {
						ArrayList<Integer> tmp = new ArrayList<Integer>();
						tmp.add(i);
						tmp.add(1);
						tmp.add(-1);
						tmp.add(1);
						result.add(tmp);
					}
				}
			}
		}
		for (int i = 0; i < result.size(); i++) {
			ArrayList<Integer> res1 = result.get(i);
			if ((res1.get(1) == res1.get(2)) && (res1.get(1) == res1.get(3))) {
				result.remove(res1);
				i--;
			}
		}
		int i=0;
		while(i < result.size()){
			ArrayList<Integer> res1 = result.get(i);
			int j=i+1;
			while(j < result.size()){
				ArrayList<Integer> res2 = result.get(j);
				if (res1.get(1) == res2.get(1) && res1.get(2) == res2.get(2) && res1.get(3) == res2.get(3)) {
					result.remove(res2);
				}
				j++;
			}
			i++;
		}
		return result;
	}

	
	
	/**
	 * 
	 * @param root
	 * @param result
	 * @return
	 */
	public List<ArrayList<String>> creatReductBDD(Node root, List<ArrayList<Integer>> result) {
		List<ArrayList<String>> reductbdd = new ArrayList<ArrayList<String>>();
		for (int i = 0; i < result.size(); i++) {
			ArrayList<Integer> res = result.get(i);
			ArrayList<String> tmp = new ArrayList<String>();
			if (res.get(3) == 0) {
				tmp.add("" + res.get(0));
				tmp.add("-1");
				tmp.add("-1");
				tmp.add("0");
			} else {
				if (res.get(3) == 1) {
					tmp.add("" + res.get(0));
					tmp.add("-1");
					tmp.add("-1");
					tmp.add("1");
				} else {
					tmp.add("" + res.get(0));
					int low = 0;
					int high = 0;
					for (int j = 0; j < i; j++) {
						ArrayList<Integer> res2 = result.get(j);
						if (res.get(1) == res2.get(3)) {
							low = res2.get(0);
						}
						if (res.get(2) == res2.get(3)) {
							high = res2.get(0);
						}
					}
					tmp.add("" + low);
					tmp.add("" + high);
					root.setMarkFalse();
					tmp.add(root.searchNode(res.get(0), null).getValue());
				}
			}
			reductbdd.add(tmp);
		}
		return reductbdd;
	}

	/**
	 * 
	 * @param n
	 * @param nodes
	 * @param result
	 */
	public void addChild(Node n, ArrayList<BDD> nodes, List<ArrayList<String>> result) {
		int idlow =0; int idhigh=0;
		Node low = null;
		Node high = null;
		for (ArrayList<String> tmp : result){
			if(n.getId()==Integer.parseInt(tmp.get(0))){
				idlow = Integer.parseInt(tmp.get(1));
				idhigh = Integer.parseInt(tmp.get(2));
			}
		}
		for (BDD node : nodes) {
			if (node.getRoot().getId() == idlow) {
				low = node.getRoot();
			}
			if (node.getRoot().getId() == idhigh) {
				high = node.getRoot();
			}
		}

		if (n instanceof InternalNode) {
			if (((InternalNode) n).getLow() == null) {
				((InternalNode) n).setLow(low);
				addChild(((InternalNode) n).getLow(), nodes, result);
			} else {
				addChild(((InternalNode) n).getLow(), nodes, result);
			}
			if (((InternalNode) n).getHigh() == null) {
				((InternalNode) n).setHigh(high);
				addChild(((InternalNode) n).getHigh(), nodes, result);
			} else {
				addChild(((InternalNode) n).getHigh(), nodes, result);
			}
			
			
		}
	}

	/**
	 * 
	 * @param result
	 * @param bdd
	 * @return
	 */
	public BDD creatReductBDD(List<ArrayList<String>> result, BDD bdd) {
		ArrayList<BDD> nodes = new ArrayList<BDD>();
		for (ArrayList<String> node : result) {
			if (node.get(1) == "-1") {
				nodes.add(new BDD(Integer.parseInt(node.get(0)), node.get(3),
						false));
			} else {
				nodes.add(new BDD(Integer.parseInt(node.get(0)), node.get(3),
						false, null, null));
			}
		}
		bdd = nodes.get(0);
		addChild(bdd.getRoot(), nodes, result);
		return bdd;
	}
	
	
	public BDD reductBDD(){
		List<ArrayList<Integer>> levels = levelOrder(getRoot());
		setMarkFalse(getRoot());
		List<ArrayList<Integer>> result = reductBDD(getRoot(),levels, 2);
		setMarkFalse(getRoot());
		List<ArrayList<String>> result2 = creatReductBDD(getRoot(),result);
		Collections.reverse(result2);
		BDD bddreduct = creatReductBDD(result2, null);
		return bddreduct;
		
	}
/*********************************************************************************************
 * 	fin reduce
 ********************************************************************************************/
	
	
	/******************************************************
	 * Logic operation between 2 BDD
	 * ***************************************************/
	public Node operation(Node u, Node v, String op, Node w, Node zeroTerminal, Node unTerminal) {
		if (op.equals("AND")) {
			if ((u instanceof ExternalNode)&& (v instanceof InternalNode)) {
				if(u.getValue().equals("1")){
					 w = v;
				}else{
					w= ((InternalNode) v).getLow();
				}
				  
			} else {
				if ((v instanceof ExternalNode)&& (u instanceof InternalNode)) {
					if(v.getValue().equals("1")){
						 w = u;
					}else{
						w= ((InternalNode) u).getLow();
					}
				} else {
					if((u instanceof InternalNode) && (v instanceof InternalNode)){
							w = new InternalNode(u.getId(), u.getValue(),false, null, v);
							if(((InternalNode) u).getLow() instanceof ExternalNode){
								((InternalNode) w).setLow(zeroTerminal);
							}else{
								((InternalNode) w).setLow(((InternalNode) u).getLow());
								Node z = ((InternalNode) u).getLow();
								
								while(((InternalNode) z).getHigh() instanceof InternalNode){
									z = ((InternalNode) z).getHigh();
								}
								((InternalNode) z).setHigh(v);
							}
						}
			    }
		     }
     	} else {
		    if (op.equals("OR")) {
		    	
		    	if ((u instanceof ExternalNode)&& (v instanceof InternalNode)) {
		    		 w=v;
				} else {
					if ((v instanceof ExternalNode)&& (u instanceof InternalNode)) {
						 w=u;
					} else {
						if((u instanceof InternalNode) && (v instanceof InternalNode)){
							if (u.getValue().equals(v.getValue())) {
								w = new InternalNode(u.getId(), u.getValue(),false, ((InternalNode) u).getLow(), null);
								((InternalNode) w).setHigh(operation(((InternalNode) u).getHigh(), ((InternalNode) v).getHigh(),"OR", null,zeroTerminal,unTerminal));
								
							} else {
								w = new InternalNode(u.getId(), u.getValue(),false, null, null);
								if(((InternalNode) u).getLow() instanceof ExternalNode){
								((InternalNode) w).setLow(v);
							}else{
							    ((InternalNode) w).setLow(operation(((InternalNode) u).getLow(), v,"OR", null,zeroTerminal,unTerminal));
							}
								
								if(((InternalNode) u).getHigh() instanceof ExternalNode){
									if(((InternalNode) v).getHigh() instanceof ExternalNode){
										((InternalNode) w).setHigh(((InternalNode) v).getHigh());
										
									}else{
										Node z = ((InternalNode) v).getHigh();
										while(z instanceof InternalNode){
											z = ((InternalNode) z).getHigh();
										}
										((InternalNode) w).setHigh(z);
									}
								}else{
									((InternalNode) w).setHigh(((InternalNode) u).getHigh());
								}
								
							}
					      }
				    }
			     }
	     	}
		    
		}
		
		return w;
	}
    /**
     * 
     * @param w
     * @return
     */
	public BDD bddOperation(Node w){
		BDD bdd = new BDD();
		if(w instanceof InternalNode){
	      bdd = new BDD(w.getId(),w.getValue(),false,((InternalNode) w).getLow(),((InternalNode) w).getHigh());	
	    }else{
	    	bdd = new BDD(w.getId(),w.getValue(),false);	
	    }
		return bdd;
	}
	
		
   /**
   * 
   * @param ids
   * @return
   */
	public AndOrTree transfertToTree(int ids){
		return root.transfertToTree(root,null,ids);
	}
}