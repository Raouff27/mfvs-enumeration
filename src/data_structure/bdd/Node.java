package data_structure.bdd;

import data_structure.and_or_trees.AndOrTree;
import utils.StringMethods;


public class Node {
	/**
	 * Attributes
	 */
	private final int id;
	private final String var;
	protected boolean mark;
	
    /**
     * Constructor 
     * @param id
     * @param val
     * @param mark
     */
	public Node(int id, String var,boolean mark) {
		this.id = id;
		this.var = var;
		this.mark = mark;  //pour marquer les noeuds qui ont d�ja parcouris 
	}
	
	/**
	 * 
	 * @return return the identifier of this node 
	 */
	public int getId() {
		return id;
	}	
	
	/**
	 * 
	 * @return return the value of this node
	 */
	public String getValue() {
		return var;
	}

	
	/**
	 * 
	 * @return return the mark of this node
	 */
	public boolean getMark() {
		return mark;
	}
    /**
     * mark this node on true
     */
	public void setMarkTrue(){
		this.mark=true;
	}
	/**
	 * mark this node on false
	 */
	public void setMarkFalse(){
		this.mark=false;
	}
	
	
	 /**
     * Returns a string representation of this node.
     *
     * @depth   The number of leading empty space in the string
     * @return  The string representation of the node
     */
	String toString(int depth) {
     		return StringMethods.spaces(depth) + toString() + '\n';
	}
	
	/**
	 * return a string used to drawing the BDD with GraphViz
	 * @return 
	 */
	public String toGraphViz() {
		return toGraphViz();
	}

	public int numberOfNodes() {
		return this.numberOfNodes();
	}

	public int hight() {
		return this.hight();
	}
	
	public Node searchNode(int id, Node m) {
		return this.searchNode(id,m);
	}


	public AndOrTree transfertToTree(Node root, data_structure.and_or_trees.AndOrTree tree, int id) {
		return this.transfertToTree(root, tree, id);
	}
	

}
