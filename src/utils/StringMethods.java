package utils;

public class StringMethods {

    public static String spaces(int n) {
        String s = "";
        for (int i = 0; i < n; ++i) {
            s += ' ';
        }
        return s;
    }
}
