package gui;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import data_structure.and_or_trees.AndOrTree;
import data_structure.and_or_trees.Node;

public class OpenTree {

	public static AndOrTree openTree(String f) {
		AndOrTree tree = new AndOrTree();
		String temp = "";
		try {
			BufferedReader tampon = new BufferedReader(new FileReader(f));

			do {
				temp = tampon.readLine();
				if (temp != null) {
					String[] readinptoken = temp.split(":");
					int id = Integer.parseInt(readinptoken[0]);
					String label = readinptoken[1];
					int parent = Integer.parseInt(readinptoken[2]);
					if (label.equals("AND")) {
						if (parent == -1) {
							tree = new AndOrTree(id, "AND",
									new ArrayList<Node>());
						} else {
							AndOrTree and = new AndOrTree(id, "AND",
									new ArrayList<Node>());
							tree.addNode(and.getRoot(), parent);
						}
					} else if (label.equals("OR")) {
						if (parent == -1) {
							tree = new AndOrTree(id, "OR",
									new ArrayList<Node>());
						} else {
							AndOrTree or = new AndOrTree(id, "OR",
									new ArrayList<Node>());
							tree.addNode(or.getRoot(), parent);
						}
					} else {
						if (parent == -1) {
							tree = new AndOrTree(id, label);
						} else {
							AndOrTree lf = new AndOrTree(id, label);
							tree.addNode(lf.getRoot(), parent);
						}
					}
				}

			} while (temp != null);
			tampon.close();
		} catch (Exception e) {
			if (e != null)
				System.err.println("Error : " + e.getMessage());
		}
		return tree;
	}

}
