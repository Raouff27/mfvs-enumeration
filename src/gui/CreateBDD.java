package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import data_structure.bdd.*;

import javax.swing.*;

import utils.GraphViz;

public class CreateBDD extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	JPanel pan = new JPanel();
	JLabel nodeIdLabel = new JLabel("Node id:");
	JLabel nodeId = new JLabel("1");
	JLabel root = new JLabel("(root)");
	JLabel typeLabel = new JLabel("Node type :");
	JLabel lowLabel = new JLabel("Low id :");
	JLabel highLabel = new JLabel("High id :");
	JLabel value = new JLabel("Node value :");
	JTextField nodeValue = new JTextField();
	JComboBox<String> nodeType = new JComboBox<String>();
	JTextField lowid = new JTextField();
	JTextField highid = new JTextField();
	JButton add = new JButton("Add");
	JButton finish = new JButton("Finish");
	int id = 1;
	ArrayList<String> nodeChildren = new ArrayList<String>();
	ArrayList<BDD> nodes = new ArrayList<BDD>();
	boolean correct = true;

	public CreateBDD() {

		setTitle("Create new And/Or tree");
		this.setPreferredSize(new Dimension(460, 255));
		setLayout(new BorderLayout());
		pan.setLayout(null);

		nodeIdLabel.setBounds(10, 20, 60, 15);
		pan.add(nodeIdLabel);
		nodeId.setBounds(70, 20, 80, 15);
		pan.add(nodeId);
		root.setBounds(90, 20, 80, 15);
		pan.add(root);

		typeLabel.setBounds(10, 50, 70, 15);
		pan.add(typeLabel);
		nodeType.addItem("Internal node");
		nodeType.addItem("0-terminal");
		nodeType.addItem("1-terminal");
		nodeType.setBounds(78, 47, 105, 22);
		pan.add(nodeType);

		lowLabel.setBounds(10, 80, 90, 15);
		pan.add(lowLabel);
		lowid.setBounds(57, 77, 60, 20);
		pan.add(lowid);
		highLabel.setBounds(10, 110, 90, 15);
		pan.add(highLabel);
		highid.setBounds(57, 107, 60, 20);
		pan.add(highid);
		value.setBounds(10, 140, 120, 15);
		pan.add(value);
		nodeValue.setBounds(82, 137, 60, 20);
		pan.add(nodeValue);

		add.setBounds(200, 180, 100, 27);
		pan.add(add);
		finish.setBounds(310, 180, 100, 27);
		pan.add(finish);

		this.getContentPane().add(pan, "Center");
		setVisible(true);
		this.setLocation(300, 240);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		add.addActionListener(this);
		finish.addActionListener(this);
		nodeType.addItemListener(null);

		nodeType.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {

				Object selected = nodeType.getSelectedItem();
				if (selected.toString().equals("Internal node")) {
					lowid.setEditable(true);
					highid.setEditable(true);
					nodeValue.setEditable(true);
					nodeValue.setText("");
				} else {
					if (selected.toString().equals("0-terminal")) {
						nodeValue.setText("0");
					} else {
						nodeValue.setText("1");
					}
					lowid.setText("");
					highid.setText("");
					lowid.setEditable(false);
					highid.setEditable(false);
					nodeValue.setEditable(false);
				}
			}
		});
	}

/*	public static void main(String[] args) {
		CreateBDD creat = new CreateBDD();
		creat.setVisible(true);
		creat.setResizable(false);
		creat.pack();
	}
*/
	public void addChild(Node n, ArrayList<BDD> nodes, ArrayList<String> nodeChildren) {
		if (n instanceof InternalNode) {
			if (((InternalNode) n).getLow() == null) {
				String[] readinptoken = nodeChildren.get(n.getId() - 1).split(
						"\\:");
				int low = Integer.parseInt(readinptoken[0]);
				if (low > nodeChildren.size()) {
					correct = false;
				} else {
					BDD bdd = nodes.get(low - 1);
					((InternalNode) n).setLow(bdd.getRoot());
					addChild(bdd.getRoot(), nodes, nodeChildren);
				}
			} else {
				addChild(((InternalNode) n).getLow(), nodes, nodeChildren);
			}

			if (((InternalNode) n).getHigh() == null) {
				String[] readinptoken = nodeChildren.get(n.getId() - 1).split(
						"\\:");
				int high = Integer.parseInt(readinptoken[1]);
				if (high > nodeChildren.size()) {
					correct = false;
				} else {
					BDD bdd = nodes.get(high - 1);
					((InternalNode) n).setHigh(bdd.getRoot());
					addChild(bdd.getRoot(), nodes, nodeChildren);
				}
			} else {
				addChild(((InternalNode) n).getHigh(), nodes, nodeChildren);
			}

		} else {
		}

	}

	public void actionPerformed(ActionEvent e) {
		Object o = e.getSource();
		if (o.equals(add)) {
			if (nodeValue.getText().equals("0")) {
				nodeChildren.add("-1:-1");
				BDD bdd = new BDD(Integer.parseInt(nodeId.getText()),
						nodeValue.getText(), false);
				nodes.add(bdd);
				nodeType.removeItem("0-terminal");
				id = id + 1;
			} else {
				if (nodeValue.getText().equals("1")) {
					nodeChildren.add("-1:-1");
					BDD bdd = new BDD(Integer.parseInt(nodeId.getText()),
							nodeValue.getText(), false);
					nodes.add(bdd);
					nodeType.removeItem("1-terminal");
					id = id + 1;
				} else {
					if (nodeValue.getText().equals("")
							|| lowid.getText().equals("")
							|| highid.getText().equals("")) {
						JOptionPane.showMessageDialog(null,
								"Error, Value messing ! ", "Error",
								JOptionPane.OK_OPTION);
					} else {
						boolean b = true;
						int low = 0;
						int high = 0;
						try {
							low = Integer.parseInt(lowid.getText());
						} catch (NumberFormatException ex) {
							b = false;
						}
						try {
							high = Integer.parseInt(highid.getText());
						} catch (NumberFormatException ex) {
							b = false;
						}
						if (b == false) {
							JOptionPane
									.showMessageDialog(
											null,
											"Error, low_id and high_id have to be integer ! ",
											"Error", JOptionPane.OK_OPTION);
						} else {
							nodeChildren.add(low + ":" + high);
							BDD bdd = new BDD(
									Integer.parseInt(nodeId.getText()),
									nodeValue.getText(), false, null, null);
							nodes.add(bdd);
							id = id + 1;
							root.setVisible(false);
						}
					}
				}
			}

			nodeId.setText("" + id);
			lowid.setText("");
			highid.setText("");
			nodeValue.setText("");
		}

		if (o.equals(finish)) {

			if (nodes.size() == 0) {
				this.setVisible(false);
			} else {

				BDD bdd = nodes.get(0);
				addChild(bdd.getRoot(), nodes, nodeChildren);

				if (correct) {
					Main.size.setText("Number of Nodes : "
							+ bdd.numberOfNodes());
					Main.haut.setText("Hight : "+bdd.hight());
					bdd.setMarkFalse(bdd.getRoot());
					Main.nbrs.setText("Number of variables : "
							+ bdd.numberOfVariables(bdd.getRoot(),new ArrayList<String>()));
					
					Main.setBDD(bdd);
					GraphViz gv = new GraphViz();
					bdd.setMarkFalse(bdd.getRoot());
					gv.add(bdd.toGraphViz());
					File out = new File("Images/bdd.png");
					gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), "png"),
							out);
					ImageIcon icon = new ImageIcon("Images/bdd.png");
					icon.getImage().flush();
					icon = new ImageIcon("Images/bdd.png");
					Main.label.setIcon(icon);
					this.setVisible(false);
				} else {
					Main.setBDD(null);
					JOptionPane
							.showMessageDialog(
									null,
									"Error, BDD not correct, check id of low and high nodes ! ",
									"Error", JOptionPane.OK_OPTION);
					this.setVisible(false);
					System.out.println("BDD not correct !");
				}
			}
		}
	}

}
