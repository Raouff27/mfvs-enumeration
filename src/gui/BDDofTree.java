package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class BDDofTree extends JFrame {
	
	private static final long serialVersionUID = 1L;
	public static JScrollPane defil;
	public static JLabel label = new JLabel();
	JPanel pan1 = new JPanel();
	JPanel pan = new JPanel();
	
	public BDDofTree() {
		setTitle("The BDD corresponding to the tree ");
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(620, 500));
		setLayout(new BorderLayout());

		pan1.setBackground(Color.white);
		pan1.setBounds(0, 0, 600, 450);
		ImageIcon imageIcon = new ImageIcon("Images/bdd.png");
		label.setIcon(imageIcon);
		imageIcon.getImage().flush();
		imageIcon = new ImageIcon("Images/bdd.png");
		label.setIcon(imageIcon);
		
		
		pan1.add(label, BorderLayout.CENTER);
		defil = new JScrollPane(pan1);
		defil.setPreferredSize(new Dimension(600, 450));
		pan.add(defil, "North");
		this.getContentPane().add(pan);
		this.setLocation(720, 200);
	}
	
	/*public static void main(String[] args) {
		Transfert bdd = new Transfert();
		bdd.setVisible(true);
		bdd.pack();
	}*/
}
