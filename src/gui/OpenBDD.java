package gui;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import data_structure.bdd.*;
public class OpenBDD {
	
	
	public static BDD openBDD(String f) {
		BDD bdd = new BDD();
		ArrayList<String> nodeChildren = new ArrayList<String>();
		ArrayList<BDD> nodes = new ArrayList<BDD>();
		String temp = "";
		try {
			BufferedReader tampon = new BufferedReader(new FileReader(f));

			do {
				temp = tampon.readLine();
				if (temp != null) {
					String[] readinptoken = temp.split(":");
					int id = Integer.parseInt(readinptoken[0]);
					String var = readinptoken[1];
					int low = Integer.parseInt(readinptoken[2]);
					int high = Integer.parseInt(readinptoken[3]);
					
					if (low == -1) {
						BDD bd = new BDD(id,var,false);
						nodes.add(bd);
						nodeChildren.add("-1:-1");
					}else {
						BDD bd = new BDD(id,var,false,null,null);
						nodes.add(bd);
						nodeChildren.add(low+":"+high);
					}
				}
			}

			 while (temp != null);
			tampon.close();
		} catch (Exception e) {
			if (e != null)
				System.err.println("ERREUR : " + e.getMessage());
		}
		bdd = nodes.get(0);
		addChild(bdd.getRoot(), nodes, nodeChildren);
		bdd.setMarkFalse(bdd.getRoot());
		return bdd;
	}

	public static void addChild(Node n, ArrayList<BDD> nodes,
			ArrayList<String> nodeChildren) {

		if (n instanceof InternalNode) {
			if (((InternalNode) n).getLow() == null) {
				String[] readinptoken = nodeChildren.get(searchNode(nodes,n.getId(),false)).split(
						"\\:");
				int low = Integer.parseInt(readinptoken[0]);
				BDD bdd = nodes.get(searchNode(nodes,low,false));
				((InternalNode) n).setLow(bdd.getRoot());
				addChild(bdd.getRoot(), nodes, nodeChildren);

			} else {
				addChild(((InternalNode) n).getLow(), nodes, nodeChildren);
			}

			if (((InternalNode) n).getHigh() == null) {
				String[] readinptoken = nodeChildren.get(searchNode(nodes,n.getId(),false)).split(
						"\\:");
				int high = Integer.parseInt(readinptoken[1]);

				BDD bdd = nodes.get(searchNode(nodes,high,false));
				((InternalNode) n).setHigh(bdd.getRoot());
				addChild(bdd.getRoot(), nodes, nodeChildren);

			} else {
				addChild(((InternalNode) n).getHigh(), nodes, nodeChildren);
			}

		} else {
		}

	}
	
	public static int searchNode(ArrayList<BDD> nodes,int id, boolean b){
		int i=0; 
		while (!b && i<nodes.size()){
			if (nodes.get(i).getRoot().getId() == id){
				b=true;
			}else{
				i++;
				}
		}
		return i;
	}

}
