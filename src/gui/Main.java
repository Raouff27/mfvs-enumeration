package gui;

import java.awt.BorderLayout;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import utils.GraphViz;
import data_structure.and_or_trees.*;
import data_structure.bdd.BDD;

public class Main extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	public static AndOrTree tree;
	public static BDD bdd;

	public static JScrollPane defil;
	public static JLabel label = new JLabel();
	JPanel pan1 = new JPanel();
	JPanel pan2 = new JPanel();
	JPanel pan2_1 = new JPanel();
	JPanel pan2_2 = new JPanel();
	JPanel pan2_3 = new JPanel();
	JPanel pan3 = new JPanel();
	JPanel pan = new JPanel();
	static JLabel size = new JLabel("Number of nodes :");
	static JLabel haut = new JLabel("Hight :");
	static JLabel nbrs = new JLabel("Number of solutions :");

	JButton values = new JButton("Values of Tree");
	JButton solutions = new JButton("Family of sets");
	JButton nodeset = new JButton("Set of solution of a node");
	JButton randomsolution = new JButton("Random solution ");
	JButton isUniform = new JButton("isUniform");
	JButton verify = new JButton("Verify if a set is a solution");
	
	
	JButton flat = new JButton("Flat Tree");
	JButton binarize = new JButton("Binarize Tree");
	JButton transfertTreeToBDD = new JButton("Convert tree to BDD");

	JButton reduct = new JButton("Reduct the BDD");
	JButton transfertBddToTree = new JButton("Convert BDD to Tree");

	static JTextArea out = new JTextArea(7, 80);

	public static void setTree(AndOrTree tree) {
		Main.tree = tree;
	}

	public static AndOrTree getTree() {
		return Main.tree;
	}

	public static void setBDD(BDD bdd) {
		Main.bdd = bdd;
	}

	public static BDD getBDD() {
		return Main.bdd;
	}

	public Main() {
		setTitle("And/Or tree data structure");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setJMenuBar(new Menu());
		this.setPreferredSize(new Dimension(1360, 720));
		setLayout(new BorderLayout());

		pan1.setBackground(Color.white);
		pan1.setBounds(0, 0, 1080, 550);
		ImageIcon imageIcon = new ImageIcon("Images/clear.png");
		label.setIcon(imageIcon);
		pan1.add(label, BorderLayout.CENTER);
		defil = new JScrollPane(pan1);
		defil.setPreferredSize(new Dimension(1080, 550));

		pan3.setLayout(new BorderLayout());
		pan3.setPreferredSize(new Dimension(1080, 120));
		Font font = new Font("Verdana", Font.BOLD, 12);
		out.setFont(font);
		JScrollPane outScroll = new JScrollPane(out);
		pan3.add(outScroll);
		pan.setLayout(new BorderLayout());
		pan.add(defil, "North");
		pan.add(pan3, "South");
		this.getContentPane().add(pan);

		pan2.setLayout(null);
		pan2.setPreferredSize(new Dimension(260, 720));

		pan2_1.setBounds(0, 0, 258, 380);
		pan2_1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		pan2_1.setBorder(BorderFactory
				.createTitledBorder(" And/Or tree Operations :"));
		pan2_1.setLayout(null);

		values.setBounds(30, 25, 180, 25);
		solutions.setBounds(30, 60, 180, 25);
		nodeset.setBounds(30, 95, 180, 25);
		randomsolution.setBounds(30, 130, 180, 25);
		isUniform.setBounds(30, 165, 180, 25);
		verify.setBounds(30, 200, 180, 25);
		
		
		
		pan2_1.add(values);
		pan2_1.add(solutions);
		pan2_1.add(nodeset);
		pan2_1.add(randomsolution);
		pan2_1.add(isUniform);
		pan2_1.add(verify);
		
		
		flat.setBounds(30, 260, 180, 25);
		binarize.setBounds(30, 295, 180, 25);
		transfertTreeToBDD.setBounds(30, 330, 180, 25);
		pan2_1.add(flat);
		pan2_1.add(binarize);
		pan2_1.add(transfertTreeToBDD);

		pan2_2.setBounds(0, 410, 260, 100);
		pan2_2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		pan2_2.setBorder(BorderFactory.createTitledBorder(" BDD Operations :"));
		pan2_2.setLayout(null);
		reduct.setBounds(30, 25, 180, 25);
		pan2_2.add(reduct);
		transfertBddToTree.setBounds(30, 60, 180, 25);
		pan2_2.add(transfertBddToTree);

		pan2_3.setBounds(0, 542, 260, 120);
		pan2_3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		pan2_3.setBorder(BorderFactory.createTitledBorder(" Informations :"));
		pan2_3.setLayout(null);
		size.setBounds(30, 35, 200, 15);
		pan2_3.add(size);
		haut.setBounds(30, 62, 200, 15);
		pan2_3.add(haut);
		nbrs.setBounds(30, 89, 200, 15);
		pan2_3.add(nbrs);

		pan2.add(pan2_1);
		pan2.add(pan2_2);
		pan2.add(pan2_3);
		this.getContentPane().add(pan2, "East");

		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		flat.addActionListener(this);
		values.addActionListener(this);
		solutions.addActionListener(this);
		nodeset.addActionListener(this);
		reduct.addActionListener(this);
		transfertBddToTree.addActionListener(this);
		transfertTreeToBDD.addActionListener(this);
		randomsolution.addActionListener(this);
		isUniform.addActionListener(this);
		binarize.addActionListener(this);
		verify.addActionListener(this);
		
	}

	public static void main(String argv[]) throws IOException {

		JFrame frame = new Main();
		frame.setResizable(false);
		frame.pack();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object o = e.getSource();

		if (o.equals(flat)) {
		if (tree !=null) tree= tree.flat();
			
		if (tree != null) {
				int i = 0, hight = tree.hight();
				while (i < hight && tree != null) {
					tree = tree.flat();
					i++;
				}
			} else {
				out.setText(out.getText() + "the tree is empty! \n");
			}
		
			Main.setTree(tree);
			if (tree == null) {
				Main.size.setText("Number of Nodes : 0");
				Main.haut.setText("Hight : 0");
				Main.nbrs.setText("Number of solutions : 0");
				ImageIcon icon = new ImageIcon("Images/clear.png");
				icon.getImage().flush();
				icon = new ImageIcon("Images/clear.png");
				Main.label.setIcon(icon);
			} else {
				System.out.println(tree.toGraphViz());
				Main.size.setText("Number of Nodes : " + tree.numberOfNodes());
				Main.haut.setText("Hight : " + tree.hight());
				Main.nbrs.setText("Number of solutions : "
						+ tree.numberOfSolutions());
				GraphViz gv = new GraphViz();
				gv.add(tree.toGraphViz());
				File out = new File("Images/tree.png");
				gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), "png"), out);

				ImageIcon icon = new ImageIcon("Images/tree.png");
				icon.getImage().flush();
				icon = new ImageIcon("Images/tree.png");

				Main.label.setIcon(icon);

			}
     
		}

		if (o.equals(values)) {
			if (Main.getTree() == null) {
				out.setText(out.getText() + "The Tree is empty ! \n");
			} else {
				out.setText(out.getText() + "Values :" + getTree().values()
						+ "\n");
			}
		}
		if (o.equals(solutions)) {
			if (Main.getTree() == null) {
				out.setText(out.getText() + "The Tree is empty ! \n");
			} else {
				ArrayList<Solution> list = new ArrayList<Solution>();
				list = getTree().getSolutions(tree);
				String st = "    ";
				for (Solution solution : list) {
					st += solution.toString() + "\n    ";
				}
				out.setText(out.getText() + "The family of solutions : \n" + st
						+ "\n");
			}

		}

		if (o.equals(nodeset)) {
			if (Main.getTree() == null) {
				out.setText(out.getText() + "The tree is empty ! \n");
			} else {
				ArrayList<String> l = Main.getTree().idList(
						new ArrayList<String>());
				Object[] tab = l.toArray();
				String id = (String) JOptionPane.showInputDialog(null,
						"select the id of the node",
						"set of solutions of a node",
						JOptionPane.QUESTION_MESSAGE, null, tab, tab[0]);
				if (id != null){
				Node node = Main.getTree().searchNode(Integer.parseInt(id),
						null);
				if (node == null) {
					JOptionPane.showMessageDialog(null,
							"Error, no node has this id ! ", "Error",
							JOptionPane.OK_OPTION);
				} else {
					AndOrTree t = null;
					if (node instanceof AndNode) {
						t = new AndOrTree(node.getId(), node.getValue(),
								((AndNode) node).children);
					} else {
						if (node instanceof OrNode) {
							t = new AndOrTree(node.getId(), node.getValue(),
									((OrNode) node).children);
						} else {
							t = new AndOrTree(node.getId(), node.getValue());
						}
					}
				
					ArrayList<Solution> list = new ArrayList<Solution>();
					list = getTree().getSolutions(t);
					String st = "    ";
					for (Solution solution : list) {
						st += solution.toString() + "\n    ";
					}
					out.setText(out.getText()
							+ "The set of solutions of the node " + id
							+ " is :\n" + st + "\n");
				}
				}
			}
		}

		if (o.equals(reduct)) {
			if(bdd == null){
				out.setText(out.getText()
						+ "The BDD is empty ! \n");
			}else{
			bdd.setMarkFalse(bdd.getRoot());
			bdd = bdd.reductBDD();
			bdd.getRoot().setMarkFalse();
		    bdd.setMarkFalse(bdd.getRoot());
			size.setText("Number of Nodes : " + bdd.numberOfNodes());
			haut.setText("Hight : " + bdd.hight());
			bdd.setMarkFalse(bdd.getRoot());
			bdd.setMarkFalse(bdd.getRoot());
			nbrs.setText("Number of variables : "
					+ bdd.numberOfVariables(bdd.getRoot(),
							new ArrayList<String>()));
			setBDD(bdd);
			GraphViz gv = new GraphViz();
			bdd.setMarkFalse(bdd.getRoot());
			gv.add(bdd.toGraphViz());
			File out = new File("Images/bdd.png");
			gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), "png"), out);
			ImageIcon icon = new ImageIcon("Images/bdd.png");
			icon.getImage().flush();
			icon = new ImageIcon("Images/bdd.png");
			label.setIcon(icon);
			}
		}
		
		
		if (o.equals(transfertTreeToBDD)) {
			if(tree==null){
				out.setText(out.getText() + "the tree is empty! \n");
			}else{
				tree = getTree().flat();
				if (tree != null) {
					int i = 0, hight = tree.hight();
					while (i < hight && tree != null) {
						tree = tree.flat();
						i++;
					}
				} else {
					out.setText(out.getText() + "the tree is empty! \n");
				}
				Main.setTree(tree);
				
				if (tree == null) {
					Main.size.setText("Number of Nodes : 0");
					Main.haut.setText("Hight : 0");
					Main.nbrs.setText("Number of solutions : 0");
					ImageIcon icon = new ImageIcon("Images/clear.png");
					icon.getImage().flush();
					icon = new ImageIcon("Images/clear.png");
					Main.label.setIcon(icon);
				} else {
					Main.size.setText("Number of Nodes : " + tree.numberOfNodes());
					Main.haut.setText("Hight : " + tree.hight());
					Main.nbrs.setText("Number of solutions : "
							+ tree.numberOfSolutions());
					GraphViz gv = new GraphViz();
					gv.add(tree.toGraphViz());
					File out = new File("Images/tree.png");
					gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), "png"), out);

					ImageIcon icon = new ImageIcon("Images/tree.png");
					icon.getImage().flush();
					icon = new ImageIcon("Images/tree.png");

					Main.label.setIcon(icon);

				}
				
				data_structure.bdd.BDD bd =tree.convertTreeToBDD();
				bd.setMarkFalse(bd.getRoot());
				System.out.println(bd.toGraphViz());
				bd.setMarkFalse(bd.getRoot());
				
				GraphViz gv = new GraphViz();
				bd.setMarkFalse(bd.getRoot());
				gv.add(bd.toGraphViz());
				File out = new File("Images/bdd.png");
				gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), "png"), out);
				
				BDDofTree showBDD = new BDDofTree();
				showBDD.setVisible(true);
				showBDD.pack();
			}
		}
		
		
		if (o.equals(transfertBddToTree)) {
			if(bdd == null){
				out.setText(out.getText()
						+ "The BDD is empty ! \n");
			}else{
			bdd.setMarkFalse(bdd.getRoot());
			bdd=bdd.reductBDD();
			
			if(bdd == null){
				out.setText(out.getText()
						+ "The BDD is empty ! \n");
			}else{
			bdd.setMarkFalse(bdd.getRoot());
			bdd = bdd.reductBDD();
			bdd.getRoot().setMarkFalse();
		    bdd.setMarkFalse(bdd.getRoot());
			size.setText("Number of Nodes : " + bdd.numberOfNodes());
			haut.setText("Hight : " + bdd.hight());
			bdd.setMarkFalse(bdd.getRoot());
			bdd.setMarkFalse(bdd.getRoot());
			nbrs.setText("Number of variables : "
					+ bdd.numberOfVariables(bdd.getRoot(),
							new ArrayList<String>()));
			setBDD(bdd);
			GraphViz gv = new GraphViz();
			bdd.setMarkFalse(bdd.getRoot());
			gv.add(bdd.toGraphViz());
			File out = new File("Images/bdd.png");
			gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), "png"), out);
			ImageIcon icon = new ImageIcon("Images/bdd.png");
			icon.getImage().flush();
			icon = new ImageIcon("Images/bdd.png");
			label.setIcon(icon);
			}
			

			
			tree =bdd.transfertToTree(1);
			System.out.println(tree.toGraphViz());
			tree = tree.flat();
			if (tree != null) {
				int i = 0, hight = tree.hight();
				while (i < hight && tree != null) {
					tree = tree.flat();
					i++;
				}
			
			GraphViz gv = new GraphViz();
			gv.add(tree.toGraphViz());
			File out = new File("Images/tree.png");
			gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), "png"), out);
			
			TreeOfBDD showBDD = new TreeOfBDD();
			showBDD.setVisible(true);
			showBDD.pack();
			}
			else{
				out.setText(out.getText() + "the tree is empty! \n");
			}
			}
			
			
			
		}
		
		
		if (o.equals(randomsolution)){
			if(tree==null){
				out.setText(out.getText() + "the tree is empty! \n");
			}else{
				ArrayList<String> solution = tree.randomSolution();
				out.setText(out.getText() + "A random solution with uniform probability :  "+ solution + "\n");
			}
		}
		
		if (o.equals(isUniform)){
			if(tree==null){
				out.setText(out.getText() + "the tree is empty! \n");
			}else{
				int k = tree.isUniform(0);
				if(k==-1){
				    out.setText(out.getText() + "The family is not uniform \n");
				}else{
					out.setText(out.getText() + "The family is uniform with a size of = "+k+"\n");
				}
			}
		}
		
		if (o.equals(verify)){
			if(tree==null){
				out.setText(out.getText() + "the tree is empty! \n");
			}else{
				ArrayList<String> solution = new ArrayList<String>();
				//solution.add("1");
				solution.add("2");
				//solution.add("3");
				//Set<String> solutionSet  = new HashSet(solution);
				//Set<String> result = tree.isSolution(solutionSet,new HashSet());
				/*if (result.equals(solutionSet)){
					out.setText(out.getText() + "the set .... is a solution \n");
					out.setText(out.getText() +result +"\n");
					
				}else{
					out.setText(out.getText() + "the set .... is not a solution \n");
				}*/
			}
		}
		if (o.equals(binarize)){
			if(tree==null){
				out.setText(out.getText() + "the tree is empty! \n");
			}else{
				Node n = tree.binarize();
				
				if (n != null) {
					if (n instanceof AndNode) {
						tree= new AndOrTree(n.getId(), n.getValue(),
								((AndNode) n).children);
					} else {
						if (n instanceof OrNode) {
						     tree = new AndOrTree(n.getId(), n.getValue(),
									((OrNode) n).children);
						} else {
							tree = new AndOrTree(n.getId(), n.getValue());
						}
					}
				} 
				Main.setTree(tree);
				if (tree == null) {
					Main.size.setText("Number of Nodes : 0");
					Main.haut.setText("Hight : 0");
					Main.nbrs.setText("Number of solutions : 0");
					ImageIcon icon = new ImageIcon("Images/clear.png");
					icon.getImage().flush();
					icon = new ImageIcon("Images/clear.png");
					Main.label.setIcon(icon);
				} else {
					System.out.println(tree.toGraphViz());
					Main.size.setText("Number of Nodes : " + tree.numberOfNodes());
					Main.haut.setText("Hight : " + tree.hight());
					Main.nbrs.setText("Number of solutions : "
							+ tree.numberOfSolutions());
					GraphViz gv = new GraphViz();
					gv.add(tree.toGraphViz());
					File out = new File("Images/tree.png");
					gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), "png"), out);

					ImageIcon icon = new ImageIcon("Images/tree.png");
					icon.getImage().flush();
					icon = new ImageIcon("Images/tree.png");

					Main.label.setIcon(icon);

				}
				
			}
		}
		
	}
}
