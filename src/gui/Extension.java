package gui;

import java.io.*;

public class Extension extends javax.swing.filechooser.FileFilter {
	String extension, description;

	// constructeur
	public Extension(String ext, String desc) {
		extension = ext;
		description = desc;
	}

	public boolean accept(File f) {
		if (f.getName().endsWith(extension))
			return true;
		else if (f.isDirectory())
			return true;
		else
			return false;
	}

	public String getDescription() {
		return description + "(*" + extension + ")";
	}
}