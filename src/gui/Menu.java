package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import utils.GraphViz;
import data_structure.and_or_trees.*;
import data_structure.bdd.*;

public class Menu extends JMenuBar implements ActionListener {

	private static final long serialVersionUID = 1L;
	private static JMenuItem mfvs, about;

	private JMenuItem creatTree = new JMenuItem("Create Tree");
	private JMenuItem openTree = new JMenuItem("Open Tree (.txt .aot)");
	private JMenu saveTree = new JMenu("Save Tree");
	private JMenuItem exit = new JMenuItem("Exit ");

	private JMenuItem creatBDD = new JMenuItem("Create BDD");
	private JMenuItem openBDD = new JMenuItem("Open BDD .txt .bdd");
	private JMenu saveBdd = new JMenu("Save BDD");

	private JMenuItem saveTreePng = new JMenuItem(".png format");
	private JMenuItem saveTreeDot = new JMenuItem(".dot format");
	private JMenuItem saveTreeAot = new JMenuItem(".aot format");
	private JMenuItem saveTreeTxt = new JMenuItem(".txt format");

	private JMenuItem saveBddPng = new JMenuItem(".png format");
	private JMenuItem saveBddDot = new JMenuItem(".dot format");
	private JMenuItem saveBddBdd = new JMenuItem(".bdd format");
	private JMenuItem saveBddTxt = new JMenuItem(".txt format");

	public Menu() {
		// the tree Menu
		JMenu menuTree = new JMenu("And/Or tree  ");
		menuTree.add(creatTree);
		saveTree.add(saveTreeAot);
		saveTree.add(saveTreeTxt);
		saveTree.add(saveTreeDot);
		saveTree.add(saveTreePng);
		menuTree.add(openTree);
		menuTree.add(saveTree);
		menuTree.addSeparator();
		menuTree.add(exit);
		creatTree.addActionListener(this);
		openTree.addActionListener(this);
		saveTreeDot.addActionListener(this);
		saveTreeAot.addActionListener(this);
		saveTreeTxt.addActionListener(this);
		saveTreePng.addActionListener(this);
		exit.addActionListener(this);
		this.add(menuTree);

		// the BDD Menu
		JMenu menuBDD = new JMenu("BDD     ");
		menuBDD.add(creatBDD);
		menuBDD.add(openBDD);
		saveBdd.add(saveBddBdd);
		saveBdd.add(saveBddTxt);
		saveBdd.add(saveBddDot);
		saveBdd.add(saveBddPng);
		menuBDD.add(saveBdd);
		creatBDD.addActionListener(this);
		openBDD.addActionListener(this);
		saveBddDot.addActionListener(this);
		saveBddBdd.addActionListener(this);
		saveBddTxt.addActionListener(this);
		saveBddPng.addActionListener(this);
		this.add(menuBDD);

		// the MFVS Menu
		JMenu menuMFVS = new JMenu("MFVS-Enumeration");
		mfvs = new JMenuItem("Select a .gml digraph ");
		mfvs.addActionListener(this);
		menuMFVS.add(mfvs);
		this.add(menuMFVS);

		// About Menu
		JMenu aboutMenu = new JMenu("About     ");
		about = new JMenuItem("About And/Or Tree");
		about.addActionListener(this);
		aboutMenu.add(about);
		this.add(aboutMenu);
	}

	private void saveTree(data_structure.and_or_trees.Node node,
			BufferedWriter buffer) {
		String st;
		if (node != null) {
			try {
				st = Main.getTree().saveTree();
				buffer.write(st, 0, st.length());
			} catch (Exception e) {
				if (e != null)
					System.err.println("ERROR : " + e.getMessage());
			}
		}
	}

	public void saveFile(File f) {
		try {
			BufferedWriter tampon = new BufferedWriter(new FileWriter(f));
			saveTree(Main.getTree().getRoot(), tampon);
			tampon.close();
		} catch (Exception e) {
			if (e.getMessage() != null)
				System.err.println("Error : " + e.getMessage());
		}
	}

	public void actionPerformed(ActionEvent e) {
		Object o = e.getSource();

		if (o.equals(mfvs)) {

			JFileChooser fileChooser = new JFileChooser("samples");
			int returnValue = fileChooser.showOpenDialog(null);
			File selectedFile = new File("");
			AndOrTree tree = new AndOrTree();
			if (returnValue == JFileChooser.APPROVE_OPTION) {
				selectedFile = fileChooser.getSelectedFile();
				String[] readinptoken = selectedFile.getName().split("\\.");
				String ext = readinptoken[1];
				if (ext.equals("gml") || ext.equals("dot")) {
					Process p = null;
					try {
						p = Runtime.getRuntime().exec(
								"python mfvs/main.py " + selectedFile.getPath()
										+ " " + ext);
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(p.getInputStream()));
						while ((reader.readLine()) != null) {
						}
						p.waitFor();

					} catch (IOException e1) {
						e1.printStackTrace();
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
					int choix = JOptionPane
							.showConfirmDialog(
									null,
									"Do you want to construct the And_Or_Tree representing the set of mfvs of \n the graph "
											+ selectedFile.getName() + " ?",
									"Confirmation", JOptionPane.YES_NO_OPTION);
					if (choix == JOptionPane.YES_OPTION) {

						tree = OpenTree.openTree("out/tree.txt");
						Main.setTree(tree);
						Main.size.setText("Number Of Nodes : "
								+ tree.numberOfNodes());
						Main.haut.setText("Hight : "+tree.hight());
						
						Main.nbrs.setText("Number of solutions : "
								+ tree.numberOfSolutions());

						GraphViz gv = new GraphViz();
						gv.add(tree.toGraphViz());
						File out = new File("Images/tree.png");
						gv.writeGraphToFile(
								gv.getGraph(gv.getDotSource(), "png"), out);

						ImageIcon icon = new ImageIcon("Images/tree.png");
						icon.getImage().flush();
						icon = new ImageIcon("Images/tree.png");

						Main.label.setIcon(icon);
					}
				} else {
					JOptionPane.showMessageDialog(null,
							"Error, the digraph format is not correct",
							"Error", JOptionPane.OK_OPTION);
				}
			}
		}

		if (o.equals(creatTree)) {
			ImageIcon icon = new ImageIcon("Images/clear.png");
			icon.getImage().flush();
			icon = new ImageIcon("Images/clear.png");
			Main.label.setIcon(icon);
			CreateTree creat = new CreateTree();
			creat.setVisible(true);
			creat.setResizable(false);
			creat.pack();
		}

		if (o.equals(creatBDD)) {
			ImageIcon icon = new ImageIcon("Images/clear.png");
			icon.getImage().flush();
			icon = new ImageIcon("Images/clear.png");
			Main.label.setIcon(icon);
			CreateBDD creat = new CreateBDD();
			creat.setVisible(true);
			creat.setResizable(false);
			creat.pack();
			if (Main.getBDD() != null) {
				System.out.println(Main.getBDD().toString());
				Main.getBDD().setMarkFalse(Main.getBDD().getRoot());
				
				Main.getBDD().setMarkFalse(Main.getBDD().getRoot());
			}
		}

		if (o.equals(openTree)) {
			JFileChooser fileChooser = new JFileChooser("samples//AndOrTrees");
			int returnValue = fileChooser.showOpenDialog(null);
			File selectedFile = new File("");
			AndOrTree tree = new AndOrTree();
			if (returnValue == JFileChooser.APPROVE_OPTION) {
				selectedFile = fileChooser.getSelectedFile();
				String[] readinptoken = selectedFile.getName().split("\\.");
				String ext = readinptoken[1];
				if (ext.equals("txt") || ext.equals("aot")) {
					tree = OpenTree.openTree(selectedFile.getPath());
					Main.setTree(tree);
					Main.size.setText("Number of Nodes : "
							+ tree.numberOfNodes());
					Main.haut.setText("Hight : "+tree.hight());
					Main.nbrs.setText("Number of solutions : "
							+ tree.numberOfSolutions());
					GraphViz gv = new GraphViz();
					gv.add(tree.toGraphViz());
					File out = new File("Images/tree.png");
					gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), "png"),
							out);

					ImageIcon icon = new ImageIcon("Images/tree.png");
					icon.getImage().flush();
					icon = new ImageIcon("Images/tree.png");

					Main.label.setIcon(icon);

				} else {
					JOptionPane.showMessageDialog(null,
							"Error, the tree format is not correct", "Error",
							JOptionPane.OK_OPTION);
				}
			}
		}
		if (o.equals(exit)) {
			System.exit(0);
		}

		if (o.equals(saveTreeAot)) {
			if(Main.getTree()==null){
				Main.out.setText(Main.out.getText() + "The And/or tree is impty ! \n");	
			}else{
			JFileChooser fileChooser = new JFileChooser("samples//AndOrTrees");
			int returnValue = fileChooser.showSaveDialog(null);
			File file = fileChooser.getSelectedFile();
			if (returnValue == JFileChooser.APPROVE_OPTION) {
				if (file != null) {
					if (file.getName().endsWith(".aot"))
						saveFile(file);
					else {
						String temp = file.getPath().concat(".aot");
						file = new File(temp);
						saveFile(file);
					}
				} else
					JOptionPane.showMessageDialog(null, "No file sellected",
							"Error", JOptionPane.ERROR_MESSAGE);
			}
			}
		}

		if (o.equals(saveTreeTxt)) {
			if(Main.getTree()==null){
				Main.out.setText(Main.out.getText() + "The And/or tree is impty ! \n");	
			}else{
			JFileChooser fileChooser = new JFileChooser("samples//AndOrTrees");
			int returnValue = fileChooser.showSaveDialog(null);
			File file = fileChooser.getSelectedFile();
			if (returnValue == JFileChooser.APPROVE_OPTION) {
				if (file != null) {
					if (file.getName().endsWith(".txt"))
						saveFile(file);
					else {
						String temp = file.getPath().concat(".txt");
						file = new File(temp);
						saveFile(file);
					}
				} else
					JOptionPane.showMessageDialog(null, "No file sellected",
							"Error", JOptionPane.ERROR_MESSAGE);
			}
			}
		}
		if (o.equals(saveTreeDot)) {
			if(Main.getTree()==null){
				Main.out.setText(Main.out.getText() + "The And/or tree is impty ! \n");	
			}else{
			JFileChooser fileChooser = new JFileChooser("samples//AndOrTrees");
			int returnValue = fileChooser.showSaveDialog(null);
			File file = fileChooser.getSelectedFile();
			if (returnValue == JFileChooser.APPROVE_OPTION) {
				if (file != null) {
					if (file.getName().endsWith(".dot")) {
						try {
							BufferedWriter tampon = new BufferedWriter(
									new FileWriter(file));
							tampon.write(Main.getTree().toGraphViz());
							tampon.close();
						} catch (Exception ex) {
							if (ex.getMessage() != null)
								System.err
										.println("Error : " + ex.getMessage());
						}
					} else {
						String temp = file.getPath().concat(".dot");
						file = new File(temp);
						try {
							BufferedWriter tampon = new BufferedWriter(
									new FileWriter(file));
							tampon.write(Main.getTree().toGraphViz());
							tampon.close();
						} catch (Exception ex) {
							if (ex.getMessage() != null)
								System.err
										.println("Error : " + ex.getMessage());
						}
					}
				} else
					JOptionPane.showMessageDialog(null, "No file sellected",
							"Error", JOptionPane.ERROR_MESSAGE);
			}
			}
		}
		/************************************/
		if (o.equals(saveTreePng)) {
			if(Main.getTree()==null){
				Main.out.setText(Main.out.getText() + "The And/or tree is impty ! \n");	
			}else{
			JFileChooser fileChooser = new JFileChooser("samples//AndOrTrees");
			int returnValue = fileChooser.showSaveDialog(null);
			File file = fileChooser.getSelectedFile();
			if (returnValue == JFileChooser.APPROVE_OPTION) {
				if (file != null) {
					if (file.getName().endsWith(".png")) {
						GraphViz gv = new GraphViz();
						gv.add(Main.getTree().toGraphViz());
						gv.writeGraphToFile(
								gv.getGraph(gv.getDotSource(), "png"), file);
					} else {
						String temp = file.getPath().concat(".png");
						file = new File(temp);
						GraphViz gv = new GraphViz();
						gv.add(Main.getTree().toGraphViz());
						gv.writeGraphToFile(
								gv.getGraph(gv.getDotSource(), "png"), file);
					}
				} else
					JOptionPane.showMessageDialog(null, "No file sellected",
							"Error", JOptionPane.ERROR_MESSAGE);
			}
			}
		}
		/***************************************************/
		if (o.equals(saveBddTxt)) {
			if (Main.getBDD() == null) {
				Main.out.setText(Main.out.getText() + "BDD is impty ! \n");
			} else {
				Main.getBDD().setMarkFalse(Main.getBDD().getRoot());
				JFileChooser fileChooser = new JFileChooser("samples//BDDs");
				int returnValue = fileChooser.showSaveDialog(null);
				File file = fileChooser.getSelectedFile();
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					if (file != null) {
						if (file.getName().endsWith(".txt")) {
							try {
								BufferedWriter tampon = new BufferedWriter(
										new FileWriter(file));
								Main.getBDD().setMarkFalse(
										Main.getBDD().getRoot());
								tampon.write(Main.getBDD().saveBDD(
										Main.getBDD().getRoot()));
								tampon.close();
							} catch (Exception ex) {
								if (ex.getMessage() != null)
									System.err.println("Error : "
											+ ex.getMessage());
							}
						} else {
							String temp = file.getPath().concat(".txt");
							file = new File(temp);
							try {
								BufferedWriter tampon = new BufferedWriter(
										new FileWriter(file));
								Main.getBDD().setMarkFalse(
										Main.getBDD().getRoot());
								tampon.write(Main.getBDD().saveBDD(
										Main.getBDD().getRoot()));
								tampon.close();
							} catch (Exception ex) {
								if (ex.getMessage() != null)
									System.err.println("Error : "
											+ ex.getMessage());
							}
						}
					} else
						JOptionPane.showMessageDialog(null,
								"No file sellected", "Error",
								JOptionPane.ERROR_MESSAGE);
				}
			}
		}

		/**********************************************************/

		if (o.equals(saveBddBdd)) {
			if (Main.getBDD() == null) {
				Main.out.setText(Main.out.getText() + "BDD is impty ! \n");
			} else {
				Main.getBDD().setMarkFalse(Main.getBDD().getRoot());
				JFileChooser fileChooser = new JFileChooser("samples//BDDs");
				int returnValue = fileChooser.showSaveDialog(null);
				File file = fileChooser.getSelectedFile();
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					if (file != null) {
						if (file.getName().endsWith(".bdd")) {
							try {
								BufferedWriter tampon = new BufferedWriter(
										new FileWriter(file));
								Main.getBDD().setMarkFalse(
										Main.getBDD().getRoot());
								tampon.write(Main.getBDD().saveBDD(
										Main.getBDD().getRoot()));
								tampon.close();
							} catch (Exception ex) {
								if (ex.getMessage() != null)
									System.err.println("Error : "
											+ ex.getMessage());
							}
						} else {
							String temp = file.getPath().concat(".bdd");
							file = new File(temp);
							try {
								BufferedWriter tampon = new BufferedWriter(
										new FileWriter(file));
								Main.getBDD().setMarkFalse(
										Main.getBDD().getRoot());
								tampon.write(Main.getBDD().saveBDD(
										Main.getBDD().getRoot()));
								tampon.close();
							} catch (Exception ex) {
								if (ex.getMessage() != null)
									System.err.println("Error : "
											+ ex.getMessage());
							}
						}
					} else
						JOptionPane.showMessageDialog(null,
								"No file sellected", "Error",
								JOptionPane.ERROR_MESSAGE);
				}
				Main.getBDD().setMarkFalse(Main.getBDD().getRoot());
			}
		}
		/**********************************************************/
		if (o.equals(saveBddDot)) {
			if (Main.getBDD() == null) {
				Main.out.setText(Main.out.getText() + "BDD is impty ! \n");
			} else {
				Main.getBDD().setMarkFalse(Main.getBDD().getRoot());
				JFileChooser fileChooser = new JFileChooser("samples//BDDs");
				int returnValue = fileChooser.showSaveDialog(null);
				File file = fileChooser.getSelectedFile();
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					if (file != null) {
						if (file.getName().endsWith(".dot")) {
							try {
								BufferedWriter tampon = new BufferedWriter(
										new FileWriter(file));
								tampon.write(Main.getBDD().toGraphViz());
								tampon.close();
							} catch (Exception ex) {
								if (ex.getMessage() != null)
									System.err.println("Error : "
											+ ex.getMessage());
							}
						} else {
							String temp = file.getPath().concat(".dot");
							file = new File(temp);
							try {
								BufferedWriter tampon = new BufferedWriter(
										new FileWriter(file));
								tampon.write(Main.getBDD().toGraphViz());
								tampon.close();
							} catch (Exception ex) {
								if (ex.getMessage() != null)
									System.err.println("Error : "
											+ ex.getMessage());
							}
						}
					} else
						JOptionPane.showMessageDialog(null,
								"No file sellected", "Error",
								JOptionPane.ERROR_MESSAGE);
				}
				Main.getBDD().setMarkFalse(Main.getBDD().getRoot());
			}
		}
		/************************************/
		/************************************/
		if (o.equals(saveBddPng)) {
			if (Main.getBDD() == null) {
				Main.out.setText(Main.out.getText() + "BDD is impty ! \n");
			} else {
				Main.getBDD().setMarkFalse(Main.getBDD().getRoot());
				JFileChooser fileChooser = new JFileChooser("samples//BDDs");
				int returnValue = fileChooser.showSaveDialog(null);
				File file = fileChooser.getSelectedFile();
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					if (file != null) {
						if (file.getName().endsWith(".png")) {
							GraphViz gv = new GraphViz();
							gv.add(Main.getBDD().toGraphViz());
							gv.writeGraphToFile(
									gv.getGraph(gv.getDotSource(), "png"), file);
						} else {
							String temp = file.getPath().concat(".png");
							file = new File(temp);
							GraphViz gv = new GraphViz();
							gv.add(Main.getBDD().toGraphViz());
							gv.writeGraphToFile(
									gv.getGraph(gv.getDotSource(), "png"), file);
						}
					} else
						JOptionPane.showMessageDialog(null,
								"No file sellected", "Error",
								JOptionPane.ERROR_MESSAGE);
				}
				Main.getBDD().setMarkFalse(Main.getBDD().getRoot());
			}
		}
		/***************************************************/

		if (o.equals(openBDD)) {
			JFileChooser fileChooser = new JFileChooser("samples//BDDs");
			int returnValue = fileChooser.showOpenDialog(null);
			File selectedFile = new File("");
			BDD bdd = new BDD();
			if (returnValue == JFileChooser.APPROVE_OPTION) {
				selectedFile = fileChooser.getSelectedFile();
				String[] readinptoken = selectedFile.getName().split("\\.");
				String ext = readinptoken[1];
				if (ext.equals("txt") || ext.equals("bdd")) {
					bdd = OpenBDD.openBDD(selectedFile.getPath());
					
					Main.size.setText("Number of Nodes : "
							+ bdd.numberOfNodes());
					Main.haut.setText("Hight : "+bdd.hight());
					bdd.setMarkFalse(bdd.getRoot());
					Main.nbrs.setText("Number of variables : "
							+ bdd.numberOfVariables(bdd.getRoot(),new ArrayList<String>()));
					
					bdd.setMarkFalse(bdd.getRoot());
					Main.setBDD(bdd);
					GraphViz gv = new GraphViz();
					gv.add(Main.getBDD().toGraphViz());
					File out = new File("Images/bdd.png");
					gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), "png"),
							out);

					ImageIcon icon = new ImageIcon("Images/bdd.png");
					icon.getImage().flush();
					icon = new ImageIcon("Images/bdd.png");

					Main.label.setIcon(icon);
					

				} else {
					JOptionPane.showMessageDialog(null,
							"Error, the tree format is not correct", "Error",
							JOptionPane.OK_OPTION);
				}
			}
		}
	}
}
