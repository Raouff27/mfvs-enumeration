package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import data_structure.and_or_trees.*;

import javax.swing.*;

import utils.GraphViz;

public class CreateTree extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	JPanel pan = new JPanel();
	JLabel idNodeLabel = new JLabel("Node id:");
	JLabel idNode = new JLabel("1  (root)");
	JLabel parentLabel = new JLabel("Parent id:");
	JLabel typeLabel = new JLabel("Node type :");
	JLabel value = new JLabel("Node value :");
	JTextField nodeValue = new JTextField();
	JComboBox<String> nodeType = new JComboBox<String>();
	JComboBox<Integer> nodeParentId = new JComboBox<Integer>();

	JButton add = new JButton("Add");
	JButton finish = new JButton("Finish");
	int id = 1;

	public CreateTree() {

		setTitle("Create new And/Or tree");
		this.setPreferredSize(new Dimension(440, 235));
		setLayout(new BorderLayout());
		pan.setLayout(null);

		idNodeLabel.setBounds(10, 20, 60, 15);
		pan.add(idNodeLabel);
		idNode.setBounds(70, 20, 80, 15);
		pan.add(idNode);
		parentLabel.setBounds(10, 50, 120, 15);
		pan.add(parentLabel);
		nodeParentId.setBounds(70, 47, 60, 20);
		pan.add(nodeParentId);
		nodeParentId.setEnabled(false);
		typeLabel.setBounds(10, 80, 70, 15);
		pan.add(typeLabel);

		nodeType.addItem("And Node");
		nodeType.addItem("Or Node");
		nodeType.addItem("Leaf");
		nodeType.setBounds(78, 77, 90, 22);
		pan.add(nodeType);
		value.setBounds(10, 110, 120, 15);
		pan.add(value);
		nodeValue.setBounds(82, 107, 60, 20);
		pan.add(nodeValue);

		add.setBounds(200, 166, 100, 27);
		pan.add(add);
		finish.setBounds(310, 166, 100, 27);
		pan.add(finish);

		this.getContentPane().add(pan, "Center");
		setVisible(true);
		this.setLocation(300, 240);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		add.addActionListener(this);
		finish.addActionListener(this);
		nodeType.addItemListener(null);

		nodeType.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {

				Object selected = nodeType.getSelectedItem();
				if (selected.toString().equals("And Node")) {
					nodeValue.setText("AND");
					nodeValue.setEditable(false);
				} else {
					if (selected.toString().equals("Or Node")) {
						nodeValue.setText("OR");
						nodeValue.setEditable(false);
					} else {
						nodeValue.setText("");
						nodeValue.setEditable(true);
						nodeValue.grabFocus();
					}
				}
			}
		});
	}
/*
	public static void main(String[] args) {
		CreateTree creat = new CreateTree();
		creat.setVisible(true);
		creat.setResizable(false);
		creat.pack();
	}
*/
	public void actionPerformed(ActionEvent e) {
		Object o = e.getSource();
		if (o.equals(add)) {
			if (nodeParentId.getItemCount() == 0) {
				if (nodeValue.getText().equals("")) {
					JOptionPane.showMessageDialog(null,
							"Error, the node must have a value ", "Error",
							JOptionPane.OK_OPTION);
				} else {
					if (nodeValue.getText().equals("AND")) {
						Main.tree = new AndOrTree(1, "AND",
								new ArrayList<Node>());
						nodeParentId.addItem(1);
					} else {
						if (nodeValue.getText().equals("OR")) {
							Main.tree = new AndOrTree(1, "OR",
									new ArrayList<Node>());
							nodeParentId.addItem(1);
						} else {
							Main.tree = new AndOrTree(1, nodeValue.getText());
							add.setEnabled(false);
						}
					}
					nodeParentId.setEnabled(true);
					id++;
					idNode.setText("" + id);
					nodeValue.setText("");
				}

			} else {
				if (nodeValue.getText().equals("")) {
					JOptionPane.showMessageDialog(null,
							"Error, the node must have a value ", "Error",
							JOptionPane.OK_OPTION);
				} else {
					if (nodeValue.getText().equals("AND")) {
						AndOrTree n = new AndOrTree(Integer.parseInt(idNode
								.getText()), "AND", new ArrayList<Node>());
						Main.tree.addNode(
								n.getRoot(), Integer.parseInt(nodeParentId
										.getSelectedItem().toString()));
						nodeParentId.addItem(id);
					} else {
						if (nodeValue.getText().equals("OR")) {
							AndOrTree n = new AndOrTree(Integer.parseInt(idNode
									.getText()), "OR", new ArrayList<Node>());
							Main.tree.addNode(n
									.getRoot(), Integer.parseInt(nodeParentId
									.getSelectedItem().toString()));
							nodeParentId.addItem(id);
						} else {
							AndOrTree n = new AndOrTree(Integer.parseInt(idNode
									.getText()), nodeValue.getText());
							Main.tree.addNode(n
									.getRoot(), Integer.parseInt(nodeParentId
									.getSelectedItem().toString()));
						}
					}
					id++;
					idNode.setText("" + id);
					nodeValue.setText("");
				}
			}

		}
		if (o.equals(finish)) {
			if (Main.getTree() == null) {
				this.setVisible(false);
			} else {
				Main.size.setText("Number Of Nodes : "
						+ Main.getTree().numberOfNodes());
				Main.haut.setText("Hight : "+Main.getTree().hight());
				Main.nbrs.setText("Number of solutions : "
						+ Main.getTree().numberOfSolutions());

				GraphViz gv = new GraphViz();
				gv.add(Main.getTree().toGraphViz());
				File out = new File("Images/tree.png");
				gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), "png"), out);
				ImageIcon icon = new ImageIcon("Images/tree.png");
				icon.getImage().flush();
				icon = new ImageIcon("Images/tree.png");
				Main.label.setIcon(icon);
				this.setVisible(false);
			}
		}

	}
}
