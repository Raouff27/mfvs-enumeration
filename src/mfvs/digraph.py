import networkx
import sys
from collections import deque
from itertools import permutations
from and_or_tree import AndOrTree

# Infinity integer
INFINITY = sys.maxint

class Digraph(networkx.DiGraph):

    # Basic method

    def __repr__(self):
        r"""
        Returns a string representation of self.

        EXAMPLE::

            >>> from digraph import Digraph
            >>> Digraph()
            Digraph of 0 nodes and 0 edges
        """
        return 'Digraph of %s nodes and %s edges' % \
               (self.number_of_nodes(), self.number_of_edges())
               
    def nodes_labels(self):
        return dict((str(n),str(self.node[n]['label'])) for n in self.nodes_iter())


    def merge(self, first, second):
        r"""
        Merges two nodes in self.

        INPUT:

        - ``first`` -- the first node to be merged
        - ``second`` -- the second node to be merged

        NOTE::

            The key of the first node is kept for the resulting merged node

        EXAMPLE::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,2], 1:[2]})
            >>> g.merge(0, 1)
            >>> g.nodes(); g.edges()
            [0, 1, 2]
            [(0, 1), (0, 2), (1, 2)]
            >>> g.nodes(); g.edges()
            [0, 2]
            [(0, 0), (0, 2)]
        """
        for p in self.predecessors_iter(second):
            self.add_edge(p, first)
        for s in self.successors_iter(second):
            self.add_edge(first, s)
        self.remove_node(second)

    def vanish(self, node):
        r"""
        Makes the given node vanish while preserving its connections.

        INPUT:

        ``node`` -- the node that should vanish

        EXAMPLE::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,2], 1:[2]})
            >>> g.vanish(1)
            >>> g.nodes(); g.edges()
            [0, 2]
            [(0, 2)]
        """
        for p in self.predecessors_iter(node):
            for s in self.successors_iter(node):
                self.add_edge(p, s)
        self.remove_node(node)

    # Circuits

    def shortest_circuit(self):
        r"""
        Returns a shortest circuit of self.

        OUTPUT:

            A list representing a circuit

        EXAMPLE::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,3], 1:[2], 2:[0], 3:[2]})
            >>> g.shortest_circuit()
            [0, 1, 2, 0]
        """
        return next(self.all_circuits_iterator())

    def _circuits_iterator_by_vertex(self, vertex, starting_vertices=None, simple=False,
                                     rooted=False, max_length=None, trivial=False,
                                     remove_acyclic_edges=True):
        r"""
        Returns an iterator over the circuits of self starting with the
        given vertex.

        INPUT:

        -  ``vertex`` - the starting vertex of the circuit.
        -  ``starting_vertices`` - iterable (default: None) on
           vertices from which the circuits must start. If None,
           then all vertices of the graph can be starting points.
           This argument is necessary if ``rooted`` is set to True.
        -  ``simple`` - boolean (default: False). If set to True,
           then only simple circuits are considered. A circuit is simple
           if the only vertex occuring twice in it is the starting
           and ending one.
        -  ``rooted`` - boolean (default: False). If set to False,
           then circuits differing only by their starting vertex are
           considered the same  (e.g. ``['a', 'b', 'c', 'a']`` and
           ``['b', 'c', 'a', 'b']``). Otherwise, all circuits are enumerated.
        -  ``max_length`` - non negative integer (default: None).
           The maximum length of the enumerated circuits. If set to None,
           then all lengths are allowed.
        -  ``trivial`` - boolean (default: False). If set to True,
           then the empty circuits are also enumerated.
        -  ``remove_acyclic_edges`` - boolean (default: True) which
           precises if the acyclic edges must be removed from the graph.
           Used to avoid recomputing it for each vertex.

        OUTPUT:

            iterator

        EXAMPLES::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,3], 1:[2], 2:[0], 3:[2]})
            >>> it = g._circuits_iterator_by_vertex(0)
            >>> it.next()
            [0, 1, 2, 0]
            >>> it.next()
            [0, 3, 2, 0]
            >>> it.next()
            [0, 1, 2, 0, 1, 2, 0]
            >>> it.next()
            [0, 1, 2, 0, 3, 2, 0]
            >>> it.next()
            [0, 3, 2, 0, 1, 2, 0]
        """
        if starting_vertices is None:
            starting_vertices = [vertex]
        # First enumerate the empty circuit
        if trivial:
            yield [vertex]
        # Then remove vertices and edges that are not part of any circuit
        if remove_acyclic_edges:
            h = self.copy()
            h.remove_edges_from(self.acyclic_edges_iter())
        else:
            h = self
        queue = deque([[vertex]])
        if max_length is None:
            max_length = INFINITY
        while queue:
            path = queue.popleft()
            # Checks if a circuit has been found
            if len(path) > 1 and path[0] == path[-1]:
                yield path
            # Makes sure that the current circuit is not too long
            # Also if a circuit has been encountered and only simple circuits are allowed,
            # Then it discards the current path
            if len(path) <= max_length and (not simple or path.count(path[-1]) == 1):
                for neighbor in h.successors_iter(path[-1]):
                    # If circuits are not rooted, makes sure to keep only the minimum
                    # circuit according to the lexicographic order
                    if rooted or neighbor not in starting_vertices or path[0] <= neighbor:
                        queue.append(path + [neighbor])

    def all_circuits_iterator(self, starting_vertices=None, simple=False,
                              rooted=False, max_length=None, trivial=False):
        r"""
        Returns an iterator over all the circuits of self starting
        with one of the given vertices. The circuits are enumerated
        in increasing length order.

        INPUT:

        -  ``starting_vertices`` - iterable (default: None) on vertices
           from which the circuits must start. If None, then all
           vertices of the graph can be starting points.
        -  ``simple`` - boolean (default: False). If set to True,
           then only simple circuits are considered. A circuit is simple
           if the only vertex occuring twice in it is the starting
           and ending one.
        -  ``rooted`` - boolean (default: False). If set to False,
           then circuits differing only by their starting vertex are
           considered the same  (e.g. ``['a', 'b', 'c', 'a']`` and
           ``['b', 'c', 'a', 'b']``). Otherwise, all circuits are enumerated.
        -  ``max_length`` - non negative integer (default: None).
           The maximum length of the enumerated circuits. If set to None,
           then all lengths are allowed.
        -  ``trivial`` - boolean (default: False). If set to True,
           then the empty circuits are also enumerated.

        OUTPUT:

            iterator

        .. NOTE::

            See also :meth:`all_simple_circuits`.

        AUTHOR:

            Alexandre Blondin Masse

        EXAMPLE::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,3], 1:[2], 2:[0], 3:[2]})
            >>> it = g.all_circuits_iterator(simple=True)
            >>> list(it)
            [[0, 1, 2, 0], [0, 3, 2, 0]]
        """
        if starting_vertices is None:
            starting_vertices = self
        # Since a circuit is always included in a given strongly connected
        # component, we may remove edges from the graph
        h = self.copy()
        h.remove_edges_from(list(self.acyclic_edges_iter()))
        # We create one circuits iterator per vertex
        # This is necessary if we want to iterate over circuits with increasing length.
        vertex_iterators = dict([(v, h._circuits_iterator_by_vertex( v
                                        , starting_vertices=starting_vertices
                                        , simple=simple
                                        , rooted=rooted
                                        , max_length=max_length
                                        , trivial=trivial
                                        , remove_acyclic_edges=False
                                        )) for v in starting_vertices])
        circuits = []
        for vi in vertex_iterators.values():
            try:
                circuit = vi.next()
                circuits.append((len(circuit), circuit))
            except(StopIteration):
                pass
        # Since we always extract a shortest path, using a heap
        # can speed up the algorithm
        from heapq import heapify, heappop, heappush
        heapify(circuits)
        while circuits:
            # We choose the shortest available circuit
            _, shortest_circuit = heappop(circuits)
            yield shortest_circuit
            # We update the circuit iterator to its next available circuit if it
            # exists
            try:
                circuit = vertex_iterators[shortest_circuit[0]].next()
                heappush(circuits, (len(circuit), circuit))
            except(StopIteration):
                pass

    # Contraction methods

    # IN0, OUT0, LOOP, IN1, OUT1

    def in0(self):
        r"""
        Removes recursively all vertices having null in-degree.

        If the removal of some vertex creates another vertex with null
        in-degree, it is removed as well.

        EXAMPLE::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,2], 1:[2], 2:[3], 3:[2,4]})
            >>> g.nodes(); g.edges()
            [0, 1, 2, 3, 4]
            [(0, 1), (0, 2), (1, 2), (2, 3), (3, 2), (3, 4)]
            >>> g.in0()
            >>> g.nodes(); g.edges()
            [2, 3, 4]
            [(2, 3), (3, 2), (3, 4)]
        """
        candidates = deque(self.nodes())
        while candidates:
            v = candidates.popleft()
            if self.has_node(v) and self.in_degree(v) == 0:
                candidates.extend(self.neighbors(v))
                self.remove_node(v)

    def out0(self):
        r"""
        Removes recursively all vertices having null in-degree.

        If the removal of some vertex creates another vertex with null
        in-degree, it is removed as well.

        EXAMPLE::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,2], 1:[2], 2:[3], 3:[2,4]})
            >>> g.nodes(); g.edges()
            [0, 1, 2, 3, 4]
            [(0, 1), (0, 2), (1, 2), (2, 3), (3, 2), (3, 4)]
            >>> g.out0()
            >>> g.nodes(); g.edges()
            [0, 1, 2, 3]
            [(0, 1), (0, 2), (1, 2), (2, 3), (3, 2)]
        """
        candidates = deque(self.nodes())
        while candidates:
            v = candidates.popleft()
            if self.has_node(v) and self.out_degree(v) == 0:
                candidates.extend(self.neighbors(v))
                self.remove_node(v)

    def loop(self):
        r"""
        Removes all vertices having a self-loop and returns the list of such
        vertices.

        OUTPUT:

            A list containing the self-loops of self.

        EXAMPLE::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,2], 1:[1,2], 2:[3], 3:[3,2,4]})
            >>> g.nodes(); g.edges()
            [0, 1, 2, 3, 4]
            [(0, 1), (0, 2), (1, 1), (1, 2), (2, 3), (3, 2), (3, 3), (3, 4)]
            >>> g.loop()
            [1, 3]
            >>> g.nodes(); g.edges()
            [0, 2, 4]
            [(0, 2)]
        """
        loop_nodes = [v for v in self.nodes() if self.has_edge(v, v)]
        self.remove_nodes_from(loop_nodes)
        return loop_nodes

    def in1(self):
        r"""
        Merge every node with in-degree equal to one with its only predecessor.

        EXAMPLE::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,2], 1:[1,2], 2:[3], 3:[3,2,4]})
            >>> g.nodes(); g.edges()
            [0, 1, 2, 3, 4]
            [(0, 1), (0, 2), (1, 1), (1, 2), (2, 3), (3, 2), (3, 3), (3, 4)]
            >>> g.in1()
            >>> g.nodes(); g.edges()
            [0, 1, 2, 3]
            [(0, 1), (0, 2), (1, 1), (1, 2), (2, 3), (3, 2), (3, 3)]
        """
        candidates = deque(self.nodes())
        while candidates:
            v = candidates.popleft()
            if self.has_node(v) and self.in_degree(v) == 1 and not self.has_edge(v, v):
                p = next(self.predecessors_iter(v))
                candidates.extend(self.neighbors(v))
                candidates.extend(self.neighbors(p))
                self.vanish(v)

    def out1(self):
        r"""
        Merge every node with out-degree equal to one with its only
        successor.

        EXAMPLE::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,2], 1:[1,2], 2:[3], 3:[3,2,4]})
            >>> g.nodes(); g.edges()
            [0, 1, 2, 3, 4]
            [(0, 1), (0, 2), (1, 1), (1, 2), (2, 3), (3, 2), (3, 3), (3, 4)]
            >>> g.out1()
            >>> g.nodes(); g.edges()
            [0, 1, 3, 4]
            [(0, 1), (0, 3), (1, 1), (1, 3), (3, 3), (3, 4)]
        """
        candidates = deque(self.nodes())
        while candidates:
            v = candidates.popleft()
            if self.has_node(v) and self.out_degree(v) == 1 and not self.has_edge(v, v):
                s = next(self.successors_iter(v))
                candidates.extend(self.neighbors(v))
                candidates.extend(self.neighbors(s))
                self.vanish(v)

    # PIE

    def acyclic_edges_iter(self):
        r"""
        Returns an iterator over the acyclic edges of self.

        An edge is called acyclic if no circuit goes through it. In other
        words, the source and the target of the edge belong to distinct
        strongly connected components.

        OUTPUT:

            An iterator

        EXAMPLE::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,2], 1:[1,2], 2:[3], 3:[3,2,4]})
            >>> list(g.acyclic_edges_iter())
            [(0, 1), (0, 2), (1, 2), (3, 4)]
        """
        sccs = networkx.strongly_connected_components(self)
        node_to_scc = dict((u,scc[0]) for scc in sccs for u in scc)
        for (u,v) in self.edges_iter():
            if node_to_scc[u] != node_to_scc[v]:
                yield (u,v)

    def pi_edges_iter(self):
        r"""
        Returns an iterator over the pi-edges of self.

        An edge `(u,v)` is called a pi-edge if `(v,u)` is also an edge. In
        other words, a pi-edge is an edge for which there is an edge going in
        the reverse direction.

        OUTPUT:

            An iterator

        EXAMPLE::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,2], 1:[1,2], 2:[3], 3:[3,2,4]})
            >>> list(g.pi_edges_iter())
            [(2, 3), (3, 2)]
        """
        for (u,v) in self.edges_iter():
            if u != v and self.has_edge(v, u):
                yield (u,v)

    def pseudo_acyclic_edges_iter(self):
        r"""
        Returns an iteratof over the pseudo-acyclic edges of self.

        An edge is called pseudo-acyclic if it is acyclic in the graph obtained
        from self by removing its pi-edges. Lin and Jou proved that those edges
        could be removed without changing the minimum feedback vertex sets.

        OUTPUT:

            An iterator

        EXAMPLE::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,2], 1:[0], 2:[3], 3:[2,1]})
            >>> list(g.pseudo_acyclic_edges_iter())
            [(0, 2), (3, 1)]
        """
        g = self.copy()
        g.remove_edges_from(self.pi_edges_iter())
        g.remove_edges_from(self.acyclic_edges_iter())
        for e in g.acyclic_edges_iter():
            yield e

    def pie(self):
        r"""
        Removes acyclic and pseudo-acyclic edges from self.

        EXAMPLE::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,2], 1:[0], 2:[3], 3:[2,1]})
            >>> g.nodes(); g.edges()
            [0, 1, 2, 3]
            [(0, 1), (0, 2), (1, 0), (2, 3), (3, 1), (3, 2)]
            >>> g.pie()
            >>> g.nodes(); g.edges()
            [0, 1, 2, 3]
            [(0, 1), (1, 0), (2, 3), (3, 2)]
        """
        self.remove_edges_from(list(self.acyclic_edges_iter()))
        self.remove_edges_from(list(self.pseudo_acyclic_edges_iter()))

    # CORE

    def pi_vertices_iter(self):
        r"""
        Returns an iterator over the pi-vertices of self.

        A vertex is called pi-vertex if it the end of some pi-edge. In other
        words, a pi-vertex is a vertex through which there is at least one
        circuit of length 2.

        OUTPUT:

            An iterator

        EXAMPLE::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,2], 1:[1,2], 2:[3], 3:[3,2,4]})
            >>> list(g.pi_vertices_iter())
            [2, 3]
        """
        for (u,_) in self.pi_edges_iter():
            yield u

    def is_clique(self, nodes):
        r"""
        Indicates whether the given nodes form a clique in self but without
        self-loops.

        A clique in a directed graph is a set of nodes that are pairwise
        connected.

        INPUT:

        ``nodes`` -- an iterable over some nodes

        OUTPUT:

            A boolean

        EXAMPLE::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,2], 1:[1,2,0], 2:[0,1,3], 3:[3,2,4]})
            >>> g.is_clique([0,1,2])
            True
            >>> g.is_clique([0,2,3])
            False
        """
        return all(self.has_edge(u, v) for (u,v) in permutations(nodes, 2))

    def core(self):
        r"""
        Removes the cores from self.

        A core is a clique in self such that at least one vertex is linked only
        to the other vertices in the clique and to no other vertex. For the
        minimum feedback vertex set problem, cores can be removed since all
        vertices belonging to it but one are needed for covering every circuit.

        OUTPUT:

            The list of nodes removed in a core

        EXAMPLE::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,2], 1:[0,2], 2:[0,1,3], 3:[2,4]})
            >>> g.nodes(); g.edges()
            [0, 1, 2, 3, 4]
            [(0, 1), (0, 2), (1, 0), (1, 2), (2, 0), (2, 1), (2, 3), (3, 2), (3, 4)]
            >>> g.core()
            [1, 2]
            >>> g.nodes(); g.edges()
            [3, 4]
            [(3, 4)]
        """
        core_nodes = []
        for u in list(self.pi_vertices_iter()):
            if u in self:
                neighbors = self.neighbors(u)
                neighborhood = neighbors + [u]
                if self.is_clique(neighborhood):
                    core_nodes.extend(neighbors)
                    self.remove_nodes_from(neighborhood)
        return core_nodes

    # DOME

    def dominated_edges_iter(self):
        r"""
        Returns an iterator over the dominated edges of self.

        An edge is called dominated if no minimum circuit goes through it. See
        Lin and Jou's article for more details.

        OUTPUT:

            An iterator

        EXAMPLE::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,2], 1:[0,2], 2:[0,1,3], 3:[2,4]})
            >>> g.dominated_edges()
            >>> list(g.dominated_edges_iter())
            [(3, 4)]
        """
        for (u,v) in self.edges_iter():
            if (u,v) not in self.pi_edges_iter():
                Pu = [w for w in self.predecessors(u) if (w,u) not in self.pi_edges_iter()]
                if all(w in self.predecessors(v) for w in Pu):
                    yield (u,v)
                else:
                    Sv = [w for w in self.successors(v) if (v,w) not in self.pi_edges_iter()]
                    if all(w in self.successors(u) for w in Sv):
                        yield (u,v)

    def dome(self):
        r"""
        Removes all dominated edges from self.

        EXAMPLE::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,2], 1:[0,2], 2:[0,1,3], 3:[2,4]})
            >>> g.nodes(); g.edges()
            [0, 1, 2, 3, 4]
            [(0, 1), (0, 2), (1, 0), (1, 2), (2, 0), (2, 1), (2, 3), (3, 2), (3, 4)]
            >>> g.dome()
            >>> g.nodes(); g.edges()
            [0, 1, 2, 3, 4]
            [(0, 1), (0, 2), (1, 0), (1, 2), (2, 0), (2, 1), (2, 3), (3, 2)]
        """
        self.remove_edges_from(list(self.dominated_edges_iter()))

    def reduce(self):
        r"""
        Reduces self applying the eight contraction operators as much as
        possible and returns the vertices in the partial solution.

        More precisely, it has been shown by Lin and Jou that to compute a MFVS
        in a digraph G, it suffices to reduce the graph and take the union of
        the partial solution and any MFVS of the reduced graph to obtain a MFVS
        for the whole graph.

        OUTPUT:

            A list of vertices

        EXAMPLE:

        The following graph is completely reducible::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,2], 1:[0,2], 2:[0,1,3], 3:[2,4]})
            >>> g.reduce()
            [2]
            >>> g.nodes(); g.edges()
            []
            []
        """
        g = self.copy()
        partial_solution = []
        while True:
            while True:
                while True:
                    # Apply the first five operators
                    while True:
                        n = self.number_of_nodes()
                        self.in0()
                        self.out0()
                        partial_solution += self.loop()
                        self.in1()
                        self.out1()
                        if self.number_of_nodes() == n: break
                    # Next apply PIE
                    # If it removes some edges, then apply the first five operators again
                    m = self.number_of_edges()
                    self.pie()
                    if self.number_of_edges() == m: break
                # Next apply CORE
                # If it removes some nodes, then apply the first five operators and
                # PIE again
                n = self.number_of_nodes()
                partial_solution += self.core()
                if self.number_of_nodes() == n: break
            # Finally apply DOME
            # If it removes some edges, then apply the first five operators, PIE
            # and CORE again
            m = self.number_of_edges()
            self.dome()
            if self.number_of_edges() == m: break
        return partial_solution

    # Minimum feedback vertex sets

    def is_fvs(self, vertices):
        r"""
        Returns True if the given vertices form a feedback vertex set of self.

        INPUT:

        ``vertices`` -- the vertices to be checked if they form a minimum
        feedback vertex set of self.

        OUTPUT:

        A boolean

        EXAMPLE::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,3], 1:[0,2], 2:[1,3], 3:[2,0]})
            >>> g.is_fvs([0,1])
            False
            >>> g.is_fvs([0,1,2])
            True
        """
        g = self.copy()
        g.remove_nodes_from(vertices)
        return networkx.is_directed_acyclic_graph(g)

    def is_minimal_fvs(self, vertices):
        r"""
        Returns True if the given vertices form a minimal feedback vertex set
        of self, i.e. a feedback vertex set such that if we remove any vertex
        from it, then the resulting subset is not a feedback vertex set

        INPUT:

        ``vertices`` -- the vertices to be checked if they form a minimal
        feedback vertex set of self.

        OUTPUT:

        A boolean

        EXAMPLE::

        """
        if not self.is_fvs(vertices):
            return False
        for i in range(len(vertices)):
            vertices_but_one = vertices[:]
            del vertices_but_one[i]
            if self.is_fvs(vertices_but_one):
                return False
        return True

    def mfvs_lower_bound(self):
        r"""
        Returns a lower bound for the minimum feedback vertex set.

        A lower bound is a number that is guaranteed to be lower than the size
        of a MFVS.

        OUTPUT:

            An integer

        EXAMPLE::

        The following graph has a lower bound of 2 since there exist two
        circuits that do not share any vertex::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,3], 1:[0,2], 2:[1,3], 3:[2,0]})
            >>> g.nodes(); g.edges()
            [0, 1, 2, 3]
            [(0, 1), (0, 3), (1, 0), (1, 2), (2, 1), (2, 3), (3, 0), (3, 2)]
            >>> g.reduce()
            []
            >>> g.mfvs_lower_bound()
            2
        """
        lb = 0
        g = self.copy()
        while g:
            lb += len(g.reduce())
            if g:
                g.remove_nodes_from(g.shortest_circuit()[:-1])
                lb += 1
        return lb

    def mfvs_initial_solution(self):
        r"""
        Returns a (relatively) small feedback vertex set giving an upper bound
        for the MFVS problem.

        OUTPUT:

            A feedback vertex set

        EXAMPLE::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,3], 1:[0,2], 2:[1,3], 3:[2,0]})
            >>> g.mfvs_initial_solution()
            [1, 3]
        """
        g = self.copy()
        solution = []
        while g:
            solution += g.reduce()
            if g.number_of_nodes() != 0:
                g.vanish(min(g.nodes(), key=lambda v: g.degree(v)))
        return solution

    def _mfvs_recursive(self, current_solution, best_solution, min_p=0, level=0, reducible=True):
        r"""
        Recursively compute a minimum feedback vertex set of self.

        INPUT:

        - ``current_solution`` -- the current partial MFVS
        - ``best_solution`` -- the best solution found so far
        - ``min_p`` -- (default: 0) a lower bound for the current solution
        - ``level`` -- (default: 0) the level of the recursive call, mostly for
          debugging
        - ``reducible`` -- (default: True) indicates whether a reduction should
          be performed

        OUTPUT:

            A list containing a minimum feedback vertex set of self
        """
        g = self.copy()
        solution = current_solution[:]
        if reducible:
            solution += g.reduce()
        sccgs = list(networkx.strongly_connected_component_subgraphs(g))
        if len(sccgs) > 1:
            for sccg in sccgs:
                solution += sccg._mfvs_recursive([], best_solution=sccg.nodes(),\
                                                 level=0, reducible=False)
            return solution
        lb = g.mfvs_lower_bound()
        if level == 0:
            min_p = lb
            best_solution = solution + g.mfvs_initial_solution()
        if len(solution) + lb > len(best_solution):
            return best_solution
        elif g.number_of_nodes() == 0:
            return solution
        # Branching
        # Left: the node v is excluded from the solution
        # Right: the node v is included in the solution
        v = max(g.nodes(), key=lambda u: g.degree(u))
        left = g.copy()
        left.remove_node(v)
        left_solution = left._mfvs_recursive(solution + [v], best_solution,\
                                             min_p=min_p, level=level+1, reducible=True)
        best_solution = min(left_solution, best_solution, key=len)
        if min_p == len(best_solution):
            return best_solution
        right = g.copy()
        right.vanish(v)
        right_solution = right._mfvs_recursive(solution[:], best_solution,\
                                               min_p=min_p, level=level+1, reducible=True)
        best_solution = min(best_solution, right_solution, key=len)
        return best_solution

    def mfvs(self):
        r"""
        Returns a minimum feedback vertex set of self.

        OUTPUT:

            A list

        EXAMPLE::

            >>> from digraph import Digraph
            >>> g = Digraph({0:[1,3], 1:[0,2], 2:[1,3], 3:[2,0]})
            >>> g.mfvs()
            [0, 2]
        """
        mfvs = self._mfvs_recursive([], self.nodes())
        return mfvs

    # Essential and useless vertices

    def is_essential(self, vertex):
        r"""
        Indicates whether the given vertex is essential, i.e. it belongs to
        every minimum feedback vertex sets of self.
        """
        if self.has_edge(vertex, vertex):
            return True
        graph = self.copy()
        graph.vanish(vertex)
        return len(graph.mfvs()) > len(self.mfvs())

    def essential_vertices_iter(self):
        r"""
        Returns an iterator over the essential vertices of self.
        """
        candidates = self.mfvs()
        for vertex in candidates:
            if self.is_essential(vertex):
                yield vertex

    def is_essential_set(self, vertices):
        graph = self.copy()
        u = vertices[0]
        for v in vertices[1:]:
            graph.merge(u, v)
        if graph.has_edge(u, u):
            return True
        else:
            graph.vanish(u)
            return len(graph.mfvs()) > len(self.mfvs())

    def essential_sets_iter(self):
        for component in networkx.weakly_connected_components(self.dominance_digraph()):
            print component
            if self.is_essential_set(component):
                yield component

    def is_useless(self, vertex):
        r"""
        Indicates whether the given vertex is useless, i.e. it does not belong
        to any minimum feedback vertex sets of self.
        """
        if self.in_degree(vertex) == 0 or self.out_degree(vertex) == 0:
            return True
        graph = self.copy()
        graph.remove_node(vertex)
        return len(graph.mfvs()) == len(self.mfvs())

    def useless_vertices_iter(self):
        r"""
        Returns an iterator over the essential vertices of self.
        """
        candidates = set(self.nodes()) - set(self.mfvs())
        for vertex in candidates:
            if self.is_useless(vertex):
                yield vertex

    # Dominance graph

    def dominance_digraph(self):
        g = Digraph()
        for u in self:
            if self.in_degree(u) == 1:
                g.add_edge(next(self.predecessors_iter(u)), u)
            if self.out_degree(u) == 1:
                g.add_edge(next(self.successors_iter(u)), u)
        return g

    # MFVS enumeration

    def all_mfvs(self, verbose=False):
        g = self.copy()
        useless_vertices = list(g.useless_vertices_iter())
        if verbose:
            print 'Useless vertices: %s' % useless_vertices
        for vertex in useless_vertices:
            g.vanish(vertex)
        essential = list(g.essential_vertices_iter())
        if verbose:
            print 'Essential vertices: %s' % essential
        if essential:
            children = [AndOrTree(e) for e in essential]
            g.remove_nodes_from(essential)
            children.append(g.all_mfvs(verbose))
            return AndOrTree(AndOrTree.AND, children)
        else:
            g.pie()
            g.dome()
            if g.number_of_nodes() == 0:
                return AndOrTree()
            ccs = list(networkx.weakly_connected_component_subgraphs(g))
            if len(ccs) > 1:
                if verbose:
                    print 'Dividing according to connected components of size %s'\
                          % [len(cc) for cc in ccs]
                children = [Digraph(cc).all_mfvs(verbose) for cc in ccs]
                return AndOrTree(AndOrTree.AND, children)
            else:
                vertex = g.nodes()[0]
                if verbose:
                    print 'Branching according to vertex %s' % vertex
                h = g.copy()
                h.remove_node(vertex)
                including = h.all_mfvs(verbose)
                h = g.copy()
                h.vanish(vertex)
                excluding = h.all_mfvs(verbose)
                return AndOrTree(AndOrTree.OR,
                           [AndOrTree(AndOrTree.AND,
                               [AndOrTree(vertex), including]),\
                            excluding]
                       )
