import sys
import os
import networkx
from networkx import write_dot
from digraph import Digraph
from and_or_tree import AndOrTree 


if (sys.argv[2] == "gml"):
    g = Digraph(networkx.read_gml(sys.argv[1]))
elif (sys.argv[2] == "dot"):
    g= Digraph(networkx.read_dot(sys.argv[1]))
tree = g.all_mfvs(verbose=False)
tree.flatten()
print(tree)
print "the and_or_tree .dot format :"
print tree.to_text(g.nodes_labels())
print tree.to_graphviz(g.nodes_labels())

"""
os.remove('out//tree.dot')
o = open("out//tree.dot","w")
o.write(tree.to_graphviz(g.nodes_labels()))
o.close()
"""
os.remove('out//tree.txt')
o = open("out//tree.txt","w")
o.write(tree.to_text(g.nodes_labels()))
o.close()
    

