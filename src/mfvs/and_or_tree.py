import itertools

# And/or trees

class AndOrTree(object):

    AND = 'AND'
    OR = 'OR'

    def __init__(self, content=None, children=None):
        self._content = content
        self._children = children
        if self._children is not None:
            self._children = [child for child in self._children\
                                    if child._content is not None]
            if len(self._children) == 1:
                self._content = self._children[0]._content
                self._children = self._children[0]._children
        self._values = None

    def __repr__(self):
        s = 'And/or tree\n'
        s += self._repr(0)
        return s

    def _repr(self, depth):
        s = '   ' * depth
        s += str(self._content) + '\n'
        if self._children is not None:
            for child in self._children:
                s += child._repr(depth + 1)
        return s
    def to_text(self,labels):
        s=""
        self.id = 0
        s += self._to_text(self, -1,labels)
        return s
    
    def _to_text(self, node, parent,labels):
        id = self.id
        s =  '%s:%s:%s\n' % \
             (id, self._node_label(node._content,labels),parent)
        self.id += 1
        if node._children is not None:
            for child in node._children:
                s += self._to_text(child, id,labels)
        return s

    
    
    def to_graphviz(self,labels):
        s = 'digraph {\n'
        self.id = 0
        s += self._to_graphviz(self, -1,labels)
        s += '}\n'
        return s
    
    def _to_graphviz(self, node, parent,labels):
        id = self.id
        s =  '  %s [label="%s", style=filled, fillcolor=%s];\n' % \
             (id, self._node_label(node._content,labels), self._node_color(node._content))
        if parent != -1:
            s += '  %s -> %s;\n' % (parent, id)
        self.id += 1
        if node._children is not None:
            for child in node._children:
                s += self._to_graphviz(child, id,labels)
        return s
    
    def _node_label(self,content, labels):
        if content =="AND" or content =="OR" or labels is None :
            return content
        else:
            for key, value in labels.iteritems() : 
                if key == str(content):
                    return value
           
    def _node_color(self, content):
        if content == AndOrTree.AND:
            return 'aquamarine'
        elif content == AndOrTree.OR:
            return 'coral'
        else:
            return 'none'
   
    def __contains__(self, element):
        pass

    def values(self):
        if self._values is None:
            if self._content == AndOrTree.AND or self._content == AndOrTree.OR:
                self._values = set(itertools.chain(*(child.values() for child in self._children)))
            else:
                self._values = set([self._content])
        return self._values

    def __iter__(self):
        if self._content == AndOrTree.AND:
            it = itertools.product(*self._children)
            for parts in it:
                yield frozenset.union(*parts)
        elif self._content == AndOrTree.OR:
            for element in itertools.chain(*self._children):
                yield element
        else:
            yield frozenset([self._content])

    def all_sets(self):
        return list(self)

    def flatten(self):
        if self._children is not None:
            more_children = []
            for child in self._children:
                child.flatten()
                if self._content == child._content:
                    more_children += child._children
                else:
                    more_children.append(child)
            self._children = more_children 
